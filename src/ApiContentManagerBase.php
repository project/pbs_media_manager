<?php

namespace Drupal\pbs_media_manager;

use DateTime;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Language\Language;
use Drupal\Core\Logger\LoggerChannelInterface;
use Drupal\Core\Queue\QueueFactory;
use Drupal\Core\Queue\QueueInterface;
use Drupal\Core\State\StateInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\datetime\Plugin\Field\FieldType\DateTimeItemInterface;
use Drupal\media\Entity\Media;
use Drupal\node\Entity\Node;
use Drupal\node\NodeInterface;
use Exception;

/**
 * Class ApiContentManagerBase.
 */
abstract class ApiContentManagerBase implements ApiContentManagerInterface {
  use StringTranslationTrait;

  /**
   * PBS Media Manager API client wrapper.
   *
   * @var \Drupal\pbs_media_manager\ApiClient
   *
   * @see \OpenPublicMedia\PbsMediaManager\Client
   */
  protected $client;

  /**
   * Entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The module handler service.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * The media manager settings config.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $config;

  /**
   * Media Manager logger.
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  protected $logger;

  /**
   * Queue service.
   *
   * @var \Drupal\Core\Queue\QueueFactory
   */
  protected $queueFactory;

  /**
   * State interface.
   *
   * @var \Drupal\Core\State\StateInterface
   */
  protected $state;

  /**
   * The file system service.
   *
   * @var \Drupal\Core\File\FileSystemInterface
   */
  protected $fileSystem;

  /**
   * ApiContentManagerBase constructor.
   *
   * @param \Drupal\pbs_media_manager\ApiClient $client
   *   Media Manager API client service.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   Entity type manager service.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler service.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   * @param \Drupal\Core\Logger\LoggerChannelInterface $logger
   *   Logger channel service.
   * @param \Drupal\Core\Queue\QueueFactory $queue_factory
   *   Queue factory service.
   * @param \Drupal\Core\State\StateInterface $state
   *   State service.
   * @param \Drupal\Core\File\FileSystemInterface $file_system
   *   The filesystem service.
   */
  public function __construct(
    ApiClient $client,
    EntityTypeManagerInterface $entity_type_manager,
    ModuleHandlerInterface $module_handler,
    ConfigFactoryInterface $config_factory,
    LoggerChannelInterface $logger,
    QueueFactory $queue_factory,
    StateInterface $state,
    FileSystemInterface $file_system
  ) {
    $this->client = $client;
    $this->entityTypeManager = $entity_type_manager;
    $this->moduleHandler = $module_handler;
    $this->config = $config_factory->get('pbs_media_manager.settings');
    $this->logger = $logger;
    $this->queueFactory = $queue_factory;
    $this->state = $state;
    $this->fileSystem = $file_system;
  }

  /**
   * {@inheritdoc}
   */
  public function getQueue(): QueueInterface {
    return $this->queueFactory->get($this->getQueueName());
  }

  /**
   * {@inheritdoc}
   */
  public function getApiClient(): ApiClient {
    return $this->client;
  }

  /**
   * Gets or creates a node based on API data.
   *
   * @param string $guid
   *   Media Manager GUID of the object to get a Node for.
   * @param string $bundle
   *   The bundle type of the item being retrieved.
   * @param string $resource
   *   The kind of resource being retrieved.
   *
   * @return \Drupal\node\NodeInterface
   *   The node to use.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  protected function getOrCreateNode(string $guid, string $bundle, string $resource): NodeInterface {
    $definition = $this->entityTypeManager->getDefinition($this->getEntityTypeId());
    $node = $this->getNodeByGuid($guid, $bundle, $resource);

    if (empty($node)) {
      $node = Node::create([
        $definition->getKey('bundle') => $bundle,
      ]);
      if ($uid = $this->config->get('api.queue_user')) {
        $node->setOwnerId($uid);
      }
      $node->enforceIsNew();
    }

    return $node;
  }

  /**
   * Attempts to get Drupal guid field for a resource.
   *
   * @param string $resource
   *   The kind of resource being retrieved.
   *
   * @return string|null
   *   The Drupal ID field of the Media Manager resource or NULL.
   */
  public function getNodeGuidField(string $resource) {
    return $this->config->get($resource . '.mappings.id');
  }

  /**
   * Attempts to get Drupal guid field for a resource.
   *
   * @param string $resource
   *   The kind of resource being retrieved.
   *
   * @return string|null
   *   The Drupal ID field of the Media Manager resource or NULL.
   */
  public function getNodeUpdatedField(string $resource) {
    return $this->config->get($resource . '.mappings.updated_at');
  }

  /**
   * Attempts to get a local node by a Media Manager ID.
   *
   * @param string $guid
   *   Media Manager GUID of the object to get a Node for.
   * @param string $bundle
   *   The bundle type of the item being retrieved.
   * @param string $resource
   *   The kind of resource being retrieved.
   *
   * @return \Drupal\node\NodeInterface|null
   *   Related node or NULL if none found.
   */
  public function getNodeByGuid(string $guid, string $bundle, string $resource): ?NodeInterface {
    $guid_field = $this->getNodeGuidField($resource);
    try {
      $definition = $this->entityTypeManager->getDefinition($this->getEntityTypeId());
      $storage = $this->entityTypeManager->getStorage($this->getEntityTypeId());
      $nodes = $storage->loadByProperties([
        $definition->getKey('bundle') => $bundle,
        $guid_field => $guid,
      ]);
    }
    catch (Exception $e) {
      // Let NULL fall through.
      $nodes = [];
    }

    $node = NULL;
    if (!empty($nodes)) {
      $node = reset($nodes);
      if (count($nodes) > 1) {
        $this->logger->error('Multiple nodes found for Media Manager
          ID {id}. Node IDs found: {nid_list}. Updating node {nid}.', [
            'id' => $guid,
            'nid_list' => implode(', ', array_keys($nodes)),
            'nid' => $node->id(),
          ]);
      }
    }

    return $node;
  }

  /**
   * Returns the ID field from a node or NULL if empty.
   *
   * @param \Drupal\node\NodeInterface $node
   *   Node to evaluate.
   *
   * @return string|null
   *   ID field value or NULL.
   */
  public static function getNodeGuid(NodeInterface $node): ?string {
    try {
      $id_field = $this->config->get('show.mappings.id');
      return $node->get($id_field)->value;
    }
    catch (Exception $e) {
      return NULL;
    }
  }

  /**
   * Indicates if a node has a non-empty ID value.
   *
   * @param \Drupal\node\NodeInterface $node
   *   Node to evaluate.
   *
   * @return bool
   *   TRUE if the node has a non-empty ID field, FALSE otherwise.
   */
  public static function nodeHasGuid(NodeInterface $node): bool {
    return !empty(self::getNodeGuid($node));
  }

  /**
   * Gets a DateTime object without microsecond precision.
   *
   * Media Manager provides microseconds in the `updated_at` field for objects,
   * but Drupal's storage does not record them. This will check for microseconds
   * in a Media Manager date string and remove them if they exist before
   * creating a DateTime object from the string.
   *
   * @param string $datetime
   *   A string representation of datetime in the Media Manager format, either
   *   Y-m-d\TH:i:s\Z or Y-m-d\TH:i:s.u\Z.
   *
   * @return \DateTime|null
   *   DateTime object without microseconds or NULL if creation fails.
   */
  public static function dateTimeNoMicroseconds(string $datetime): ?DateTime {
    // If the format is Y-m-d\TH:i:s.u\Z, the length will be 27 characters and
    // the last eight should be striped to remove microseconds.
    if (strlen($datetime) == 27) {
      $datetime = substr($datetime, 0, -8) . 'Z';
    }
    try {
      $object = new DateTime($datetime);
    }
    catch (Exception $e) {
      $object = NULL;
    }
    return $object;
  }

  /**
   * Convert images array to key by profile and enforce site scheme.
   *
   * This is necessary because some images provided by Media Manager use an
   * "http" scheme. This will cause mixed media errors and prevent images from
   * loading because the website uses an "https" scheme.
   *
   * @param array $images
   *   Images from a Media Manager query.
   * @param string $image_key
   *   Images array key containing the image URL.
   * @param string $profile_key
   *   Images array key containing the image profile string.
   *
   * @return array
   *   All valid images keyed by profile string using a "\\" scheme to match the
   *   site scheme.
   */
  public function parseImages(
    array $images,
    string $image_key = 'image',
    string $profile_key = 'profile'
  ): array {
    $images = array_column($images, $image_key, $profile_key);
    foreach ($images as $key => $image) {
      $parts = parse_url($image);
      if ($parts === FALSE || !isset($parts['host']) || !isset($parts['path'])) {
        unset($images[$key]);
      }
      else {
        $images[$key] = sprintf('//%s%s', $parts['host'], $parts['path']);
      }
    }
    return $images;
  }

  /**
   * Gets latest `updated_at` field from an API response object.
   *
   * This method accounts for the `updated_at` fields in the images array for a
   * an item. These updated dates do not bubble to the top level of the item
   * for some reason. Others, e.g. "availabilities" and "geo" do bubble up.
   *
   * @param object $item
   *   API object.
   *
   * @return \DateTime|null
   *   Latest `updated_at` field value or NULL if DateTime create fails.
   */
  public static function getLatestUpdatedAt(object $item): ?DateTime {
    $updated_at = self::dateTimeNoMicroseconds($item->attributes->updated_at);

    if ($item->type == 'episode' || $item->type == 'special') {
      // An episode's images are contained in its assets.
      if (!empty($item->attributes->assets)) {
        foreach($item->attributes->assets as $asset) {
          if (isset($asset->attributes->images)) {
            foreach ($asset->attributes->images as $image) {
              // Check for updated images.
              $image_updated_at = self::dateTimeNoMicroseconds($image->updated_at);
              if ($image_updated_at > $updated_at) {
                $updated_at = $image_updated_at;
              }
            }
          }
        }
      }
    }
    else {
      if (isset($item->attributes->images)) {
        foreach ($item->attributes->images as $image) {
          $image_updated_at = self::dateTimeNoMicroseconds($image->updated_at);
          if ($image_updated_at > $updated_at) {
            $updated_at = $image_updated_at;
          }
        }
      }
    }

    return $updated_at;
  }

  /**
   * Creates a image media item based on the configured field values.
   *
   * @param object $image
   *   Image from Media Manager, including profile, image (URL), and updated_at.
   * @param string $image_title
   *   A title for the image.
   * @param string $type
   *   The resource type, such as "show" or "episode".
   * @param object $media_image
   *   The existing image, if updating.
   * @param bool $force
   *   Whether to force an update.
   *
   * @return string|null
   *   A Drupal media image id or null.
   */
  protected function addOrUpdateMediaImage($image, $image_title, $type, $media_image, $force = FALSE) {
    // Get required configuration.
    $image_media_type = $this->config->get($type . 's.image_media_type');
    if (empty($image_media_type)) {
      $this->logger->error('Please configure the media image type used on ' . $type . 's.');
      return;
    }
    $image_field = $this->config->get($type . 's.mappings.' . $image->profile);
    if (empty($image_field)) {
      $this->logger->error('Please configure the ' . $type . ' image field.');
      return;
    }
    $image_field_mappings = $this->config->get($type . 's.image_field_mappings');
    $image_title_field = $image_field_mappings['image_title'];
    $image_image_field = $image_field_mappings['image_field'];
    $image_updated_field = $image_field_mappings['image_updated'];
    if ($image_title_field == 'unused' || $image_image_field == 'unused' ||
      $image_updated_field == 'unused') {
      $this->logger->error('Please configure all 3 fields for ' . $type . ' images.');
      return;
    }

    // Reformat the updated at field.
    $updated_dt = self::dateTimeNoMicroseconds($image->updated_at);

    // Determines if the file needs to be downloaded and saved.
    $add_file = FALSE;

    // Check to see if a media image already exists in Drupal with that name.
    $media_storage = $this->entityTypeManager->getStorage('media');
    if ($media_image && $drupal_updated_str = $media_image->{$image_updated_field}->value) {
      // Image is a required field...so if its somehow empty, try to update it.
      if (empty($media_image->{$image_field})) {
        $add_file = TRUE;
      }
      $drupal_updated_dt = self::dateTimeNoMicroseconds($drupal_updated_str);
      if ($force || $updated_dt->getTimestamp() > $drupal_updated_dt->getTimestamp()) {
        // If the media item exists, delete all of the referenced image files.
        if ($image_references = $media_image->{$image_field}) {
          foreach ($image_references as $image_reference) {
            $file_id = $image_reference->get('target_id')->getValue();
            if ($referenced_file = $this->entityTypeManager->getStorage('file')->load($file_id)) {
              $referenced_file->delete();
            }
          }
        }
        // Remove the references to the images on the media item.
        $media_image->{$image_field} = NULL;
        $add_file = TRUE;
      }
    }
    // Otherwise, create a new image media entity.
    else {
      // Create a media entity.
      $media_image = $media_storage->create([
        $image_title_field => $image_title,
        'bundle' => $image_media_type,
        'langcode' => Language::LANGCODE_NOT_SPECIFIED,
        $image_updated_field => $updated_dt->format(DateTimeItemInterface::DATETIME_STORAGE_FORMAT),
      ]);
      $add_file = TRUE;

      if ($uid = $this->config->get('api.queue_user')) {
        $media_image->setOwnerId($uid);
      }
      // Must save before adding image file in order to use mid in the filename.
      $media_image->save();
    }

    if ($add_file) {
      // Attach the image file.
      $file = $this->saveImagefile($image, $media_image);
      if (is_object($file) && !empty($file->id())) {
        $media_image->set($image_image_field, [
          'target_id' => $file->id(),
          'alt' => $image_title,
        ]);
      }

      $media_image->save();
    }

    return $media_image->id();
  }

  /**
   * Creates a Drupal image file.
   *
   * @param object $image
   *   Image from Media Manager, including profile, image (URL), and updated_at.
   * @param object $media_image
   *   The media image entity.
   *
   * @return \Drupal\file\FileInterface|false
   *   A file entity, or FALSE on error.
   */
  protected function saveImagefile(object $image, object $media_image) {

    // Create the directory from today's date as YYYY/MM/DD.
    $directory = 'public://pbs_media_manager_images/' . date('Y/m/d');
    $this->fileSystem->prepareDirectory($directory, FileSystemInterface::CREATE_DIRECTORY);

    // Download the file.
    try {
      $file_data = $this->client->request('GET', $image->image);
    }
    catch (\Exception $e) {
      if ($e->getMessage()) {
        $this->logger->error($this->t('There is no image at @url.', [
          '@url' => $image->image,
        ]));
      }
      return FALSE;
    }

    // Save the image.
    $file = \Drupal::service('file.repository')->writeData($file_data->getBody(), $directory . "/" . $media_image->id() . '-' . basename($image->image), FileSystemInterface::EXISTS_REPLACE);

    return $file;
  }

  /**
   * Adds or updates Drupal video media data with Media Manager API data.
   *
   * @param object $item
   *   Media Manager API data for an Asset.
   * @param bool $force
   *   Whether or not to "force" the update. If FALSE (default) and a current
   *   Video Content node is found with a matching or newer "last_updated"
   *   value, the existing node will not be updated. If TRUE, "last_updated" is
   *   ignored.
   *
   * @return \Drupal\media\Entity\Media|null
   *   The Drupal video media entity.
   */
  public function addOrUpdateAssetContent($item, $force = FALSE) {

    // Check for required configuration.
    $video_media_type = $this->config->get('videos.video_media_type');
    if (empty($video_media_type)) {
      $this->logger->error('Please configure the video media type.');
      return;
    }
    $mappings = $this->config->get('videos.mappings');

    $media_storage = $this->entityTypeManager->getStorage('media');
    $id_field = $mappings['id'];
    $title_field = $mappings['title'];
    $attributes = $item->attributes;
    if ($media_video = $media_storage->loadByProperties([
      $id_field => $item->id,
    ])) {
      if (count($media_video) > 1) {
        $this->logger->error(
          $this->t('More than one video with the @id (@title) exists. Please delete duplicate videos.', [
            '@id' => $item->id,
            '@title' => $attributes->title,
          ]));
        return;
      }
      $media_video = reset($media_video);
    }
    // Otherwise, create download the file and create a new video media entity.
    else {
      $media_video = $media_storage->create([
        $title_field => $attributes->title,
        'bundle' => $video_media_type,
        'langcode' => Language::LANGCODE_NOT_SPECIFIED,
      ]);
      $media_video->enforceIsNew();
    }
    $operation = $this->getOperation($item, $media_video, $mappings, $force);
    $logging = $this->config->get('logging.operations');

    if ($operation == 'configured') {
      $this->logger->error($this->t('Please map the title, id, and updated_at fields for videos.'));
      return;
    }

    if ($operation == 'skipped') {
      if (!$logging['skipped']) {
        $this->logger->debug($this->t('The asset @title has not changed', [
          '@title' => $media_video->getName(),
        ]));
      }
      return $media_video;
    }
    if ($operation == 'ignored') {
      if (!$logging['ignored']) {
        $this->logger->debug($this->t('The asset @id was ignored due to a hook_pbs_media_manager_operation_alter() implementation.', [
          '@id' => $item->id,
        ]));
      }
      return;
    }

    // Media Manager video ID.
    $media_video->set($id_field, $item->id);

    // Media Manager updated date.
    $updated_dt = self::dateTimeNoMicroseconds($item->attributes->updated_at);
    $updated_at_field = $mappings['updated_at'];
    $media_video->set(
      $updated_at_field,
      $updated_dt->format(DateTimeItemInterface::DATETIME_STORAGE_FORMAT)
    );

    // Availability updated dates are father straightforward.
    $availabilities = $attributes->availabilities;
    $station_members_updated_at = $mappings['station_members_updated_at'];
    if (!empty($station_members_updated_at) && $station_members_updated_at !== 'unused') {
      $station_updated_dt = self::dateTimeNoMicroseconds($availabilities->station_members->updated_at);
      $media_video->set(
        $station_members_updated_at,
        $station_updated_dt->format(DateTimeItemInterface::DATETIME_STORAGE_FORMAT)
      );
    }
    $public_updated_at = $mappings['public_updated_at'];
    if (!empty($public_updated_at) && $public_updated_at !== 'unused') {
      $pub_updated_dt = self::dateTimeNoMicroseconds($availabilities->public->updated_at);
      $media_video->set(
        $public_updated_at,
        $pub_updated_dt->format(DateTimeItemInterface::DATETIME_STORAGE_FORMAT)
      );
    }
    $all_members_updated_at = $mappings['all_members_updated_at'];
    if (!empty($all_members_updated_at) && $all_members_updated_at !== 'unused') {
      $all_updated_dt = self::dateTimeNoMicroseconds($availabilities->all_members->updated_at);
      $media_video->set(
        $all_members_updated_at,
        $all_updated_dt->format(DateTimeItemInterface::DATETIME_STORAGE_FORMAT)
      );
    }

    $media_video = $this->addAvailabilityStartEnd(
      $media_video,
      $mappings['station_members_avail_start'],
      $mappings['station_members_avail_end'],
      $availabilities->station_members->start,
      $availabilities->station_members->end
    );
    $media_video = $this->addAvailabilityStartEnd(
      $media_video,
      $mappings['public_avail_start'],
      $mappings['public_avail_end'],
      $availabilities->public->start,
      $availabilities->public->end
    );
    $media_video = $this->addAvailabilityStartEnd(
      $media_video,
      $mappings['all_members_avail_start'],
      $mappings['all_members_avail_end'],
      $availabilities->all_members->start,
      $availabilities->all_members->end
    );

    $media_video = $this->addContentReferences($media_video, $attributes->parent_tree);

    // Process images.
    if ($images = $attributes->images) {
      foreach ($images as $image) {
        if (!empty($mappings[$image->profile]) && $image_field = $mappings[$image->profile]) {
          if ($image_field != 'unused') {
            // Check to see if this item has an image already.
            $media_image = NULL;
            if ($media_video->get($image_field)->target_id) {
              // Get the existing image entity.
              $image_id = $media_video->get($image_field)->target_id;
              $media_storage = $this->entityTypeManager->getStorage('media');
              $media_image = $media_storage->load($image_id);
            }
            if ($media_id = $this->addOrUpdateMediaImage(
              $image,
              $attributes->title . ": " . $image->profile,
              'video',
              $media_image,
              $force
            )) {
              $media_video->set($image_field, ['target_id' => $media_id]);
            }
          }
        }
      }
    }

    $remaining_fields = [
      'description_long',
      'description_short',
      'duration',
      'encored_on',
      'object_type',
      'premiered_on',
      'slug',
      'title_sortable',
      'player_code',
      'title',
      'language',
      'drm_enabled'
    ];
    foreach ($remaining_fields as $field) {
      $mapped_field = !empty($mappings[$field]) ? $mappings[$field] : NULL;
      if ($attributes->{$field} &&
        $mapped_field && $mapped_field !== 'unused' &&
        $media_video->hasField($mapped_field)
      ) {
        $media_video->set($mapped_field, $attributes->{$field});
      }
    }

    if ($uid = $this->config->get('api.queue_user')) {
      $media_video->setOwnerId($uid);
    }

    if (!$logging['changed']) {
      $this->logger->notice($this->t('The asset @title has been @operation', [
        '@title' => $media_video->getName(),
        '@operation' => $operation,
      ]));
    }

    $media_video->save();

    return $media_video;
  }

  /**
   * Adds configured availabilitiy data to media video.
   *
   * @param \Drupal\media\Entity\Media $media_video
   *   The Drupal video media entity.
   * @param string $start_field
   *   The start field.
   * @param string $end_field
   *   The end field.
   * @param string $avail_start
   *   The availabilities start date string.
   * @param string $avail_end
   *   The availabilities end date string.
   *
   * @return \Drupal\media\Entity\Media
   *   The Drupal video media entity.
   */
  protected function addAvailabilityStartEnd(
    Media $media_video,
    $start_field,
    $end_field,
    $avail_start,
    $avail_end
  ) {

    // Begin with the availability start.
    if (!empty($start_field) && $start_field!=='unused'){
      if (!empty($avail_start)) {
        $start_dt = self::dateTimeNoMicroseconds($avail_start);
        // Next, check the availability end.
        if (!empty($end_field) && $end_field !== 'unused') {
          if (!empty($avail_end)){
            $end_dt = self::dateTimeNoMicroseconds($avail_end);
            // TODO: Remove this check once this issues is committed to core:
            // https://www.drupal.org/project/drupal/issues/2794481
            if ($this->moduleHandler->moduleExists('optional_end_date')) {
              // If start and end need to go in the same field add them together.
              $media_video->set(
                $start_field,
                [
                  'value' => $start_dt->format(DateTimeItemInterface::DATETIME_STORAGE_FORMAT),
                  'end_value' => $end_dt->format(DateTimeItemInterface::DATETIME_STORAGE_FORMAT),
                ]
              );
              return $media_video;
            }
            // Otherwise, add the end in its own field.
            else {
              $media_video->set(
                $end_field,
                $end_dt->format(DateTimeItemInterface::DATETIME_STORAGE_FORMAT)
              );
            }
          } else {
            $media_video->set(
              $end_field,
              NULL
            );
          }
        }
        $media_video->set(
          $start_field,
          $start_dt->format(DateTimeItemInterface::DATETIME_STORAGE_FORMAT)
        );
      } else {
        $media_video->set(
          $start_field,
          NULL
        );
      }
    }
    return $media_video;
  }

  /**
   * Adds show, season, and episode references to media video.
   *
   * @param \Drupal\media\Entity\Media $media_video
   *   The Drupal video media entity.
   * @param object $parent_tree
   *   Parent tree data from the Media Manager Asset.
   *
   * @return \Drupal\media\Entity\Media
   *   The Drupal video media entity.
   */
  protected function addContentReferences(Media $media_video, $parent_tree) {
    $video_references = $this->config->get('videos.references');
    $tree = $this->parseParentTree($parent_tree);
    // Allows episode nodes to be referenced back from media videos
    // when the parent tree has a special but doesn't have an episode.
    if (!isset($tree['episode']) && isset($tree['special'])) {
      $tree['episode'] = $tree['special'];
    }

    foreach ($tree as $type => $guid) {
      $bundle = $this->config->get($type . 's.drupal_' . $type . '_content');
      if (isset($video_references[$type]) && $ref = $video_references[$type]) {
        if (!empty($guid) && !empty($bundle) && $ref != 'unused') {
          if ($node = $this->getNodeByGuid($guid, $bundle, $type . 's')) {
            if (!empty($node->id())) {
              $media_video->set($ref, ['target_id' => $node->id()]);
            }
          }
        }
      }
    }
    return $media_video;
  }

  /**
   * Parse a Media Manager API Asset parent tree.
   *
   * @param object $parent_tree
   *   Parent tree data from the Media Manager Asset.
   *
   * @return array
   *   Ordered array keyed by parent object types and their Media Manager ID
   *   values. E.g. --
   *
   * @code
   * <?php
   * $tree = [
   *   'episode' => 'e358cacc-86e6-46a8-9269-dc72c8e34143',
   *   'season' => 'da41c844-f87e-4c89-ae40-f41c72ebdd0f',
   *   'show' => '294a8c40-1d69-4711-9163-5d17872b40e2',
   *   'franchise' => 'e08bf78d-e6a3-44b9-b356-8753d01c7327',
   * ];
   * @endcode
   */
  protected function parseParentTree(object $parent_tree): array {
    $tree = [];

    // The initial parent will only ever be "franchise", "show", "special",
    // "season", or "episode".
    if (isset($parent_tree->type)) {
      $tree[$parent_tree->type] = $parent_tree->id;
    }
    $branch = $parent_tree->attributes;

    // "episode" will have a "season".
    if (isset($branch->season)) {
      $tree['season'] = $branch->season->id;
      $branch = $branch->season->attributes;
    }

    // "special" or "season" will have a "show".
    if (isset($branch->show)) {
      $tree['show'] = $branch->show->id;
      $branch = $branch->show->attributes;
    }

    // "show" _might_ have a "franchise".
    if (isset($branch->franchise)) {
      $tree['franchise'] = $branch->franchise->id;
    }

    return $tree;
  }

  /**
   * Determines what operation to perform on a Drupal entity based on API data.
   *
   * @param object $item
   *   Media Manager API item (shows, season, episode, or asset).
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The Drupal entity (shows, season, episode, or asset).
   * @param array $mappings
   *   The field mappings for this resource.
   * @param bool $force
   *   Whether or not to "force" the update.
   *
   * @return string
   *   The operation to be performed: 'created', 'updated', 'skipped', or
   *   'configured'. Other modules can set it to 'ignored'.
   */
  protected function getOperation($item, EntityInterface $entity, array $mappings, $force) {

    // Default to skipped. This should only be unchanged resources.
    $operation = 'skipped';

    // Confirm that the title, id, and updated_at fields are mapped.
    $title_field = $mappings['title'];
    $id_field = $mappings['id'];
    $updated_at_field = $mappings['updated_at'];
    $drupal_updated_at = $entity->get($updated_at_field)->value;
    if (empty($title_field) || $title_field == 'unused' ||
      empty($id_field) || $id_field == 'unused' ||
      empty($updated_at_field) || $updated_at_field == 'unused') {
      $operation = 'configured';
    }
    // Always return 'created' for new entities.
    elseif (is_object($entity) && $entity->IsNew()) {
      $operation = 'created';
    }
    // If 'force' is requested and it is not a new entity, then the entity needs
    // to be updated.
    elseif ($force) {
      $operation = 'updated';
    }
    // Always return 'updated' if the updated_at field on the entity is empty.
    elseif (empty($drupal_updated_at)) {
      $operation = 'updated';
    }
    // For existing Drupal entities, check if the item has changed in PBS.
    elseif ($pbs_updated_ts = self::getLatestUpdatedAt($item)->getTimestamp()) {
      // Get the updated_at time of the Drupal entity in UTC time.
      $dt_updated = new DrupalDateTime($drupal_updated_at, DateTimeItemInterface::STORAGE_TIMEZONE);
      $drupal_updated_ts = $dt_updated->getTimestamp();
      // If the updated time in PBS is greater than the updated time of the
      // Drupal entity, then the entity needs to be updated.
      if ($pbs_updated_ts > $drupal_updated_ts) {
        $operation = 'updated';
      }
    }

    // Allow other modules to alter the operation (e.g. change it to 'ignored').
    $context = [
      'item' => $item,
      'entity' => $entity,
    ];
    $this->moduleHandler->alter('pbs_media_manager_operation', $operation, $context);

    return $operation;
  }

}
