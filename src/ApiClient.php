<?php

namespace Drupal\pbs_media_manager;

use Drupal\Core\Config\ConfigFactoryInterface;
use GuzzleHttp\Client as GuzzleClient;
use OpenPublicMedia\PbsMediaManager\Client;
use OpenPublicMedia\PbsMediaManager\Exception\BadRequestException;

/**
 * Class ApiClient.
 *
 * @package Drupal\pbs_media_manager
 */
class ApiClient extends Client {

  /**
   * Config key for the API key.
   */
  const CONFIG_KEY = 'api.key';

  /**
   * Config key for the API secret.
   */
  const CONFIG_SECRET = 'api.secret';

  /**
   * Config key for the API base endpoint.
   */
  const CONFIG_BASE_URI = 'api.base_uri';

  /**
   * API key.
   *
   * @var string
   */
  private $key;

  /**
   * API secret.
   *
   * @var string
   */
  private $secret;

  /**
   * API base endpoint.
   *
   * @var string
   */
  public $baseUri;

  /**
   * Media Manager immutable config object.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $config;

  /**
   * The client for PBS.
   *
   * @var \GuzzleHttp\Client
   */
  protected GuzzleClient $client;

  /**
   * ApiClient constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   Drupal's config factory.
   */
  public function __construct(ConfigFactoryInterface $configFactory) {
    $this->config = $configFactory->get('pbs_media_manager.settings');
    $this->key = $this->config->get(self::CONFIG_KEY);
    $this->secret = $this->config->get(self::CONFIG_SECRET);

    $base_uri = $this->config->get(self::CONFIG_BASE_URI);
    if ($base_uri === 'staging') {
      $base_uri = self::STAGING;
    }
    elseif ($base_uri === 'live') {
      $base_uri = self::LIVE;
    }
    else {
      $base_uri = self::STAGING;
    }
    $this->baseUri = $base_uri;

    parent::__construct($this->key, $this->secret, $this->baseUri);
  }

  /**
   * Gets the API client key.
   *
   * @return string
   *   Media Manager API key setting.
   */
  public function getApiKey(): string {
    return $this->key;
  }

  /**
   * Gets the API client secret.
   *
   * @return string
   *   Media Manager API secret setting.
   */
  public function getApiSecret(): string {
    return $this->secret;
  }

  /**
   * Gets the API client base endpoint.
   *
   * @return string
   *   Media Manager API endpoint setting.
   */
  public function getApiEndPoint(): string {
    return $this->baseUri;
  }

  /**
   * Indicates if the API client is configured.
   *
   * @return bool
   *   TRUE if the key, secret, and endpoint are set. FALSE otherwise.
   */
  public function isConfigured(): bool {
    return (
      !empty($this->getApiKey()) &&
      !empty($this->getApiSecret()) &&
      !empty($this->getApiEndPoint())
    );
  }

  /**
   * Sends a test query to the API and returns an error or "OK".
   *
   * @return string
   *   An error string or "OK" (indicating that the connection is good).
   */
  public function testConnection(): string {
    $result = 'OK';

    try {
      $this->request('get', 'genres');
    }
    catch (BadRequestException $e) {
      $error = json_decode($e->getMessage());
      if (isset($error->detail)) {
        $result = $error->detail;
      }
      else {
        $result = (string) $error;
      }
    }

    return $result;
  }

  /**
   * Get show attributes from show_guid.
   *
   * @param string $show_guid
   *   Show guid.
   *
   * @return mixed
   *   Array of show attributes or FALSE.
   */
  public function getShowAttributes(string $show_guid) {
    if (empty($show_guid)) {
      return FALSE;
    }
    $show = $this->getShow($show_guid);
    if (!empty($show)) {
      $attributes = $show->attributes;
      return $attributes;
    }
    return FALSE;
  }

  /**
   * Get show title from guid.
   *
   * @param string $show_guid
   *   Show guid.
   *
   * @return string
   *   Show title.
   */
  public function getShowTitleFromGuid(string $show_guid) {
    $show_attributes = $this->getShowAttributes($show_guid);
    if ($show_attributes) {
      return $show_attributes->title;
    }
  }

}
