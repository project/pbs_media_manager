<?php

namespace Drupal\pbs_media_manager\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\pbs_media_manager\ApiClient;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Test controller for the pbs_media_manager module.
 */
class ApiTestController extends ControllerBase {

  /**
   * The PBS Media Manager client.
   *
   * @var \Drupal\pbs_media_manager\ApiClient
   */
  protected $client;

  /**
   * Constructs the ApiTestController.
   *
   * @param \Drupal\pbs_media_manager\ApiClient $api_client
   *   The Media Manager API service.
   */
  public function __construct(ApiClient $api_client) {
    $this->client = $api_client;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('pbs_media_manager.api_client')
    );
  }

  /**
   * Sends a test query to the API and returns result.
   *
   * @throws \Exception
   */
  public function testConnection() {

    $response = $this->client->testConnection();

    $message = $response;
    if ($response == "OK") {
      $message = 'The API test succeeded.';
    }

    return [
      '#theme' => 'item_list',
      '#title' => 'Test Result',
      '#items' => [$message],
    ];
  }

}
