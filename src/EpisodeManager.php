<?php

namespace Drupal\pbs_media_manager;

use DateTime;
use Drupal\datetime\Plugin\Field\FieldType\DateTimeItemInterface;
use Drupal\node\NodeInterface;
use Drupal\taxonomy\Entity\Term;
use Exception;
use OpenPublicMedia\PbsMediaManager\Exception\BadRequestException;
use stdClass;

/**
 * Class EpisodeManager.
 *
 * @package Drupal\pbs_media_manager
 */
class EpisodeManager extends ApiContentManagerBase {

  /**
   * {@inheritdoc}
   */
  public static function getEntityTypeId(): string {
    return 'node';
  }

  /**
   * {@inheritdoc}
   */
  public function getBundleId(): string {
    return $this->config->get('episodes.drupal_episode_content');
  }

  /**
   * {@inheritdoc}
   */
  public static function getQueueName(): string {
    return 'pbs_media_manager.queue.episodes';
  }

  /**
   * {@inheritdoc}
   */
  public function getLastUpdateTime(): DateTime {
    return new DateTime();
  }

  /**
   * {@inheritdoc}
   */
  public function setLastUpdateTime(DateTime $time): void {
  }

  /**
   * {@inheritdoc}
   */
  public function resetLastUpdateTime(): void {
  }

  /**
   * {@inheritdoc}
   */
  public static function getAutoUpdateConfigName(): string {
    return '';
  }

  /**
   * {@inheritdoc}
   */
  public static function getAutoUpdateIntervalConfigName(): string {
    return '';
  }

  /**
   * {@inheritdoc}
   *
   * @throws \Exception
   */
  public function updateQueue(DateTime $since = NULL): bool {
    $dt_start = new DateTime();
    $logging = $this->config->get('logging.operations');

    // Initialize counter for tracking items added to queue.
    $count = 0;

    $show_vocabulary = $this->config->get('shows.show_vocabulary');
    if (!empty($show_vocabulary) && $show_vocabulary != 'unused') {
      $taxonomy_manager = $this->entityTypeManager->getStorage('taxonomy_term');
      $subscribed_shows = $taxonomy_manager->loadByProperties([
        'vid' => $show_vocabulary,
      ]);
      foreach ($subscribed_shows as $show) {
        $show_guid = $show->get('field_show_guid')->value;
        // Initialize counter for tracking items added to the queue per show.
        $count_per_show = 0;
        try {
          $seasons = $this->client->getSeasons($show_guid);
        } catch (BadRequestException $e) {
          // Only log show errors in ShowManager.
        }
        if (is_object($seasons)) {
          foreach ($seasons as $season) {
            if ($episodes = $this->client->getEpisodes($season->id)) {
              foreach ($episodes as $episode) {
                if ($episode_item = $this->client->getEpisode($episode->id)) {
                  if (is_object($episode_item)) {
                    $this->getQueue()->createItem($episode_item);
                    $count++;
                    $count_per_show++;
                    // Give an incremental update every 50 episodes to avoid timeouts for a show like PBS NewsHour with many episodes.
                    if ($count_per_show % 50 === 0 && !$logging['queue']) {
                      $this->logger->debug('{show} update: {count} episodes added to the queue.',
                        [
                          'count' => $count_per_show,
                          'show' => $show->getName()
                        ]);
                    }
                  }
                }
              }
              if ($count_per_show && !$logging['queue']) {
                $this->logger->debug('{count} episodes have been added to the queue for show {show}.',
                  [
                    'count' => $count_per_show,
                    'show' => $show->getName()
                  ]);
              }
            }
          }

        }
      }
    }
    else {
      $shows = $this->client->getShows(['fetch-related' => FALSE]);
      foreach ($shows as $show) {
        // Initialize counter for tracking items added to the queue per show.
        $count_per_show = 0;
        try {
          $seasons = $this->client->getSeasons($show->id);
        } catch (\Exception $e) {
          // If we don't have access to the season, just skip it.
          continue;
        }
        foreach ($seasons as $season) {
          $episodes = $this->client->getEpisodes($season->id);
          foreach ($episodes as $episode) {
            $episode_item = $this->client->getEpisode($episode->id);
            if (is_object($episode_item)) {
              $this->getQueue()->createItem($episode_item);
              $count++;
              $count_per_show++;
              // Give an incremental update every 50 episodes to avoid timeouts for a show like PBS NewsHour with many episodes.
              if ($count_per_show % 50 === 0 && !$logging['queue']) {
                $this->logger->debug('{show} update: {count} episodes added to the queue.',
                  [
                    'count' => $count_per_show,
                    'show' => $show->attributes->title
                  ]);
              }
            }
          }
          if ($count_per_show && !$logging['queue']) {
            $this->logger->debug('{count} episodes have been added to the queue for show {show}.',
              [
                'count' => $count_per_show,
                'show' => $show->attributes->title
              ]);
          }

        }
      }
    }

    if (!$logging['queue']) {
      $this->logger->notice('{count} total episodes have been added to the queue.', ['count' => $count,]);
    }


    $this->setLastUpdateTime($dt_start);

    return TRUE;
  }

  /**
   * Gets an API response for a single Episode.
   *
   * @param string $guid
   *   ID of the Episode to get.
   *
   * @return object|null
   *   Episode item data from the API or NULL.
   */
  public function getEpisode(string $guid): ?stdClass {
    return $this->client->getEpisode($guid);
  }

  /**
   * Get Episode nodes with optional properties.
   *
   * @param array $properties
   *   Properties to filter Episode nodes.
   *
   * @return \Drupal\node\NodeInterface[]
   *   Episodes nodes.
   */
  public function getEpisodeNodes(array $properties = []): array {
    try {
      $definition = $this->entityTypeManager->getDefinition(self::getEntityTypeId());
      $storage = $this->entityTypeManager->getStorage(self::getEntityTypeId());
      $nodes = $storage->loadByProperties([
        $definition->getKey('bundle') => $this->getBundleId(),
      ] + $properties);
    }
    catch (Exception $e) {
      // Let NULL fall through.
      $nodes = [];
    }

    return $nodes;
  }

  /**
   * Gets a Episode node by TMS ID.
   *
   * @param string $id
   *   TMS ID to query with.
   *
   * @return \Drupal\node\NodeInterface|null
   *   Episode node with TMS ID or NULL if none found.
   */
  public function getEpisodeNodeByTmsId(string $id): ?NodeInterface {
    if ($tms_id_field = $this->config->get('episodes.mappings.tms_id')) {
      $nodes = $this->getEpisodeNodes([$tms_id_field => $id]);

      $node = NULL;
      if (!empty($nodes)) {
        $node = reset($nodes);
        if (count($nodes) > 1) {
          $this->logger->error('Multiple nodes found for Media Manager
            TMS ID {id}. Node IDs found: {nid_list}. Using node {nid}.', [
              'id' => $id,
              'nid_list' => implode(', ', array_keys($nodes)),
              'nid' => $node->id(),
            ]);
        }
      }
    }
    else {
      $this->logger->error('Mapping the episode TMS ID field is required.');
    }
    return $node;
  }

  /**
   * Gets a Episode node by slug.
   *
   * @param string $slug
   *   Slug to query with.
   *
   * @return \Drupal\node\NodeInterface|null
   *   Episode node with the slug or NULL if none found.
   */
  public function getEpisodeNodeBySlug(string $slug): ?NodeInterface {
    $node = NULL;
    if ($slug_field = $this->config->get('episodes.mappings.slug')) {
      $nodes = $this->getEpisodeNodes([$slug_field => $slug]);
      if (!empty($nodes)) {
        $node = reset($nodes);
      }
    }
    return $node;
  }

  /**
   * Adds or updates a Episode node based on API data.
   *
   * @param object $item
   *   An API response object for the Episode.
   * @param bool $force
   *   Whether or not to "force" the update. If FALSE (default) and a current
   *   episode content node is found with a matching or newer "last_updated"
   *   value, the existing node will not be updated. If TRUE, "last_updated" is
   *   ignored.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityStorageException
   * @throws \Exception
   */
  public function addOrUpdateEpisode(object $item, $force = FALSE): void {

    $node = $this->getOrCreateNode($item->id, $this->getBundleId(), 'episodes');
    $mappings = $this->config->get('episodes.mappings');
    $logging = $this->config->get('logging.operations');
    $operation = $this->getOperation($item, $node, $mappings, $force);

    if ($operation == 'configured') {
      $this->logger->error($this->t('Please map the title, id, and updated_at fields for episodes.'));
      return;
    }
    // Do not consider videos for ignored episodes.
    if ($operation == 'ignored') {
      if (!$logging['ignored']) {
        $this->logger->debug($this->t('The episode @id was ignored due to a hook_pbs_media_manager_operation_alter() implementation.', [
          '@id' => $item->id,
        ]));
      }
      return;
    }

    $attributes = $item->attributes;
    if (empty($attributes->title)) {
      return;
    }
    $node->setTitle($attributes->title);

    // We need to save the node before creating the videos because episodes
    // reference videos and videos reference episodes. Therefore, we also have
    // to set the title of the episode. Additionally, we will also need the
    // guid because addOrUpdateAssetContent() looks up nodes by guid.
    if ($node->isNew()) {
      if (!empty($item->id) && $mappings['id'] != 'unused') {
        $node->set($mappings['id'], $item->id);
      }
      $node->save();
    }

    // Regardless of whether or not the episode was updated or not, also check
    // the assests to determine if they have been updated.
    if ($video_field = $mappings['video']) {
      if (!empty($video_field) && $video_field !== 'unused' && !empty($attributes->assets)) {
        $videos = $this->client->get('episodes/' . $item->id . "/assets");
        $video_drupal_ids = [];
        foreach ($videos as $video) {
          if ($media_video = $this->addOrUpdateAssetContent($video, $force)) {
            $video_drupal_ids[] = ['target_id' => $media_video->id()];
          }
        }
        $node->set($video_field, $video_drupal_ids);
        // Even if the Episode has not changed, we added or updated videos, so
        // save those to the Episode.
        // TODO: Determine the operation performed and only save the episode if
        // it has changed.
        $node->save();
      }
    }

    if ($operation == 'skipped') {
      if (!$logging['skipped']) {
        $this->logger->debug($this->t('The episode @title has not changed', [
          '@title' => $node->getTitle(),
        ]));
      }
      return;
    }

    $type = $mappings['type'];
    $episode_type_vocab = $this->config->get('episodes.references.episode_type_vocabularly');
    if (!empty($item->type) && !empty($type) && $type != 'unused') {
      $taxonomy_storage = $this->entityTypeManager->getStorage('taxonomy_term');
      if ($term = $taxonomy_storage->loadByProperties([
        'name' => $item->type,
        'vid' => $episode_type_vocab,
      ])) {
        $term = reset($term);
      }
      else {
        $term = Term::create([
          'name' => $item->type,
          'vid' => $episode_type_vocab,
        ]);
        $term->save();
      }
      $node->set($type, ['target_id' => $term->id()]);
    }
    $other_attributes = [
      'tms_id',
      'ordinal',
      'description_long',
      'description_short',
      'nola',
      'premiered_on',
      'slug',
      'encored_on',
      'language',
    ];
    foreach ($other_attributes as $attribute) {
      $attribute_field = $mappings[$attribute];
      if (!empty($attributes->{$attribute}) && !empty($attribute_field) && $attribute_field != 'unused') {
        $node->set($attribute_field, $attributes->{$attribute});
      }
    }

    $updated_at = self::getLatestUpdatedAt($item);
    if ($mappings['updated_at'] != 'unused') {
      $node->set(
        $mappings['updated_at'],
        $updated_at->format(DateTimeItemInterface::DATETIME_STORAGE_FORMAT)
      );
    }

    // Process images.
    if (!empty($attributes->assets)) {
      foreach($attributes->assets as $asset) {
        // There can be multiple "related" assets.
        // First check for a full length asset, then fall back to the first
        // image available.
        if (!empty($asset->attributes->images)) {
          if (isset($asset->attributes->object_type) && $asset->attributes->object_type == 'full_length') {
            $images = $asset->attributes->images;
            break;
          }
          elseif (empty($images)) {
            $images = $asset->attributes->images;
          }
        }
      }
    }

    if (isset($images) && is_array($images)) {
      foreach ($images as $image) {
        if (!empty($mappings[$image->profile]) && $image_field = $mappings[$image->profile]) {
          if ($image_field != 'unused') {
            // Check to see if this item has an image already.
            $media_image = NULL;
            if ($node->get($image_field)->target_id) {
              // Get the existing image entity.
              $image_id = $node->get($image_field)->target_id;
              $media_storage = $this->entityTypeManager->getStorage('media');
              $media_image = $media_storage->load($image_id);
            }
            if ($media_id = $this->addOrUpdateMediaImage(
              $image,
              $attributes->title . ": " . $image->profile,
              'episode',
              $media_image
            )) {
              $node->set($image_field, ['target_id' => $media_id]);
            }
          }
        }
      }
    }

    // Add the show reference, if configured.
    $show_ref_field = $this->config->get('episodes.references.show');
    $show_guid = $attributes->show->id;
    $show_bundle = $this->config->get('shows.drupal_show_content');
    if (!empty($show_guid) && !empty($show_bundle) && $show_ref_field != 'unused') {
      $show_node = $this->getNodeByGuid($show_guid, $show_bundle, 'shows');
      if (!empty($show_node) && !empty($show_node->id())) {
        $node->set($show_ref_field, ['target_id' => $show_node->id()]);
      }
    }

    // Add the season reference, if configured.
    $season_ref_field = $this->config->get('episodes.references.season');
    $season_guid = NULL;
    if (isset($attributes->season->id)) {
      $season_guid = $attributes->season->id;
    }
    $season_bundle = $this->config->get('seasons.drupal_season_content');
    if (!empty($season_guid) && !empty($season_bundle) && $season_ref_field != 'unused') {
      $season_node = $this->getNodeByGuid($season_guid, $season_bundle, 'seasons');
      if (!empty($season_node) && !empty($season_node->id())) {
        $node->set($season_ref_field, ['target_id' => $season_node->id()]);
      }
    }

    // Save the node.
    $node->save();

    if (!empty($node->getTitle()) & !empty($operation && !$logging['changed'])) {
      $this->logger->notice($this->t('Episode @title has been @operation', [
        '@title' => $node->getTitle(),
        '@operation' => $operation,
      ]));
    }
  }

}
