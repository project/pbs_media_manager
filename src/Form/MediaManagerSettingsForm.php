<?php

namespace Drupal\pbs_media_manager\Form;

use Drupal\Core\Config\ConfigFactory;
use Drupal\Core\Datetime\DateFormatterInterface;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\pbs_media_manager\ApiClient;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class MediaManagerSettingsForm.
 *
 * @ingroup pbs_media_manager
 */
class MediaManagerSettingsForm extends ConfigFormBase {

  /**
   * Media Manager API client.
   *
   * @var \Drupal\pbs_media_manager\ApiClient
   */
  protected $apiClient;

  /**
   * Date formatting service.
   *
   * @var \Drupal\Core\Datetime\DateFormatterInterface
   */
  protected $dateFormatter;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The entity field manager.
   *
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  protected $entityFieldManager;

  /**
   * Constructs a new MediaManagerSettingsForm.
   *
   * @param \Drupal\Core\Config\ConfigFactory $config_factory
   *   Config factory service.
   * @param \Drupal\pbs_media_manager\ApiClient $api_client
   *   Media Manager API client service.
   * @param \Drupal\Core\Datetime\DateFormatterInterface $date_formatter
   *   Date formatter service.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Entity\EntityFieldManagerInterface $entity_field_manager
   *   The entity field manager.
   */
  public function __construct(
    ConfigFactory $config_factory,
    ApiClient $api_client,
    DateFormatterInterface $date_formatter,
    EntityTypeManagerInterface $entity_type_manager,
    EntityFieldManagerInterface $entity_field_manager
  ) {
    parent::__construct($config_factory);
    $this->apiClient = $api_client;
    $this->dateFormatter = $date_formatter;
    $this->entityTypeManager = $entity_type_manager;
    $this->entityFieldManager = $entity_field_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('pbs_media_manager.api_client'),
      $container->get('date.formatter'),
      $container->get('entity_type.manager'),
      $container->get('entity_field.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'pbs_media_manager_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames(): array {
    return ['pbs_media_manager.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    $config = $this->config('pbs_media_manager.settings');

    $form['api'] = [
      '#type' => 'details',
      '#title' => $this->t('Media Manager API settings'),
      '#open' => TRUE,
    ];

    $form['api']['key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('API key'),
      '#description' => $this->t('PBS Media Manager API key.'),
      '#default_value' => $this->apiClient->getApiKey(),
    ];

    $form['api']['secret'] = [
      '#type' => 'textfield',
      '#title' => $this->t('API secret'),
      '#description' => $this->t('PBS Media Manager API secret.'),
      '#default_value' => $this->apiClient->getApiSecret(),
    ];

    $form['api']['base_uri'] = [
      '#type' => 'select',
      '#title' => $this->t('Base endpoint'),
      '#description' => $this->t('PBS Media Manager API base endpoint.'),
      '#options' => [
        'staging' => $this->t('Staging'),
        'live' => $this->t('Production'),
      ],
    ];

    $base_uri = $this->apiClient->getApiEndPoint();
    if ($base_uri == ApiClient::LIVE) {
      $form['api']['base_uri']['#default_value'] = 'live';
    }
    else {
      $form['api']['base_uri']['#default_value'] = 'staging';
    }

    // Create an array of all Drupal users.
    $users = $this->entityTypeManager->getStorage('user')->loadMultiple();
    $all_users = [];
    foreach ($users as $user) {
      $all_users[$user->id()] = $user->getDisplayName();
    }
    unset($all_users[0]);
    asort($all_users);
    $form['api']['queue_user'] = [
      '#type' => 'select',
      '#title' => 'Drupal author of queued items',
      '#default_value' => $config->get('api.queue_user'),
      '#options' => $all_users,
    ];

    $form['logging'] = [
      '#type' => 'details',
      '#title' => $this->t('Logging Options'),
      '#description' => $this->t('By default, this module is verbose, and will create log entries whenever items are created, updated, skipped or ignored. You can disable that logging here.'),
      '#open' => TRUE,
    ];

    $form['logging']['operations'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Operations'),
      '#options' => [
        'queue' => $this->t('Disable logging when items have been added to the queue.'),
        'changed' => $this->t('Disable logging when items have been created or updated.'),
        'skipped' => $this->t('Disable logging when items have been skipped because they have not changed since the last update.'),
        'ignored' => $this->t('Disable logging when items have been ignored. (See: hook_pbs_media_manager_operation_alter() for more information.)')
      ],
      '#default_value' => $config->get('logging.operations')
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    $values = $form_state->getValues();
    $config = $this->config('pbs_media_manager.settings');

    $config->set(ApiClient::CONFIG_KEY, $values['key']);
    $config->set(ApiClient::CONFIG_SECRET, $values['secret']);
    $config->set(ApiClient::CONFIG_BASE_URI, $values['base_uri']);
    $config->set('api.queue_user', $values['queue_user']);
    $config->set('logging.operations', $values['operations']);

    $config->save();

    parent::submitForm($form, $form_state);
  }

}
