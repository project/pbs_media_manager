<?php

namespace Drupal\pbs_media_manager\Form;

use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class EpisodeMappingsForm.
 *
 * @ingroup pbs_media_manager
 */
class EpisodeMappingsForm extends ConfigFormBase {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The entity field manager.
   *
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  protected $entityFieldManager;

  /**
   * Constructs a new EpisodeMappingsForm.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Entity\EntityFieldManagerInterface $entity_field_manager
   *   The entity field manager.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, EntityFieldManagerInterface $entity_field_manager) {
    $this->entityTypeManager = $entity_type_manager;
    $this->entityFieldManager = $entity_field_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('entity_field.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'pbs_media_manager_episode_mappings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames(): array {
    return ['pbs_media_manager.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $config = $this->config('pbs_media_manager.settings');
    $episode_content_type = $config->get('episodes.drupal_episode_content');

    $form['episode_sync_enable'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable incremental updates to local Episode nodes
        from Media Manager data.'),
      '#default_value' => $config->get('episodes.episode_sync_enable'),
      '#return_value' => TRUE,
    ];

    $form['episodes'] = [
      '#type' => 'details',
      '#title' => $this->t('Episode content type'),
      '#open' => TRUE,
      '#states' => [
        'visible' => [
          'input[name="episode_sync_enable"]' => ['checked' => TRUE],
        ],
      ],
    ];

    // Episode content type.
    $drupal_content_types = array_keys($this->entityTypeManager->getStorage('node_type')->loadMultiple());
    $content_type_options = array_combine($drupal_content_types, $drupal_content_types);
    $form['episodes']['drupal_episode_content'] = [
      '#type' => 'select',
      '#title' => $this->t('Drupal episode content type'),
      '#description' => $this->t('Select the Drupal content type to use for Media Manager episode content.'),
      '#default_value' => $config->get('episodes.drupal_episode_content'),
      '#options' => $content_type_options,
    ];

    // Episode mappings.
    $form['episode_field_mappings'] = [
      '#type' => 'details',
      '#title' => $this->t('episode field mappings'),
      '#open' => FALSE,
      '#states' => [
        'visible' => [
          'input[name="episode_sync_enable"]' => ['checked' => TRUE],
        ],
      ],
    ];
    $required_episode_fields = [
      'title',
      'id',
      'updated_at',
    ];
    if (!empty($config->get('episodes.drupal_episode_content'))) {
      $episode_fields = array_keys($this->entityFieldManager
        ->getFieldDefinitions('node', $episode_content_type));
      $episode_field_options = ['unused' => 'unused'] +
        array_combine($episode_fields, $episode_fields);
      $pbs_episode_fields = $config->get('episodes.mappings');
      foreach (array_keys($pbs_episode_fields) as $field_name) {
        $required = in_array($field_name, $required_episode_fields) ? TRUE : FALSE;
        $form['episode_field_mappings'][$field_name] = [
          '#type' => 'select',
          '#title' => $field_name,
          '#options' => $episode_field_options,
          '#required' => $required,
          '#default_value' => $pbs_episode_fields[$field_name],
        ];
      }
    }
    else {
      $form['episode_field_mappings']['mappings_required'] = [
        '#type' => 'item',
        '#markup' => 'Select and save Drupal episode content type (above) to choose field mappings.',
      ];
    }

    // Image media type configuration.
    $form['image_settings'] = [
      '#type' => 'details',
      '#title' => $this->t('Image settings'),
      '#open' => FALSE,
      '#states' => [
        'visible' => [
          'input[name="episode_sync_enable"]' => ['checked' => TRUE],
        ],
      ],
    ];
    $media_types = array_keys($this->entityTypeManager->getStorage('media_type')->loadMultiple());
    $media_type_options = array_combine($media_types, $media_types);
    $image_media_type = $config->get('episodes.image_media_type');
    $form['image_settings']['image_media_type'] = [
      '#type' => 'select',
      '#title' => $this->t('Drupal image media type'),
      '#default_value' => $image_media_type,
      '#options' => $media_type_options,
    ];
    // Media image field mappings.
    if (!empty($image_media_type)) {
      $form['image_settings']['image_field_mappings'] = [
        '#type' => 'details',
        '#title' => $this->t('Image field mappings'),
        '#open' => TRUE,
      ];
      $image_media_fields = array_keys($this
        ->entityFieldManager
        ->getFieldDefinitions('media', $image_media_type));
      $image_field_options = ['unused' => 'unused'] +
        array_combine($image_media_fields, $image_media_fields);
      $pbs_image_fields = $config->get('episodes.image_field_mappings');
      foreach ($pbs_image_fields as $pbs_image_field => $field_value) {
        $form['image_settings']['image_field_mappings'][$pbs_image_field] = [
          '#type' => 'select',
          '#title' => $pbs_image_field,
          '#options' => $image_field_options,
          '#default_value' => $pbs_image_fields[$pbs_image_field],
          '#required' => TRUE,
        ];
      }
    }
    else {
      $form['image_settings']['image_field_mappings']['mappings_required'] = [
        '#type' => 'item',
        '#markup' => 'Select and save the Drupal image media type to choose field mappings.',
      ];
    }

    // Episode references.
    $form['episode_references'] = [
      '#type' => 'details',
      '#title' => $this->t('Episode references'),
      '#open' => FALSE,
      '#states' => [
        'visible' => [
          'input[name="episode_sync_enable"]' => ['checked' => TRUE],
        ],
      ],
    ];
    if (!empty($config->get('episodes.drupal_episode_content'))) {
      $form['episode_references']['show_reference'] = [
        '#type' => 'select',
        '#title' => 'Show reference field',
        '#options' => $episode_field_options,
        '#default_value' => $config->get('episodes.references.show'),
      ];
      $form['episode_references']['season_reference'] = [
        '#type' => 'select',
        '#title' => 'Season reference field',
        '#options' => $episode_field_options,
        '#default_value' => $config->get('episodes.references.season'),
      ];
      $vocabs = array_keys($this->entityTypeManager->getStorage('taxonomy_vocabulary')->loadMultiple());
      $vocabulary_options = array_combine($vocabs, $vocabs) + ['unused' => 'unused'];
      $form['episode_references']['episode_type_vocabularly'] = [
        '#type' => 'select',
        '#title' => 'Episode type vocabularly',
        '#options' => $vocabulary_options,
        '#default_value' => $config->get('episodes.references.episode_type_vocabularly'),
      ];
    }

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();
    $config = $this->config('pbs_media_manager.settings');

    $config->set('episodes.episode_sync_enable', $values['episode_sync_enable']);
    $config->set('episodes.drupal_episode_content', $values['drupal_episode_content']);
    $config->set('episodes.image_media_type', $values['image_media_type']);

    $pbs_episode_fields = $config->get('episodes.mappings');
    foreach (array_keys($pbs_episode_fields) as $field) {
      if (!empty($values[$field])) {
        $config->set('episodes.mappings.' . $field, $values[$field]);
      }
    }

    // Image settings.
    $config->set('episodes.image_media_type', $values['image_media_type']);
    $pbs_image_fields = $config->get('episodes.image_field_mappings');
    foreach ($pbs_image_fields as $field_name => $field_value) {
      if (isset($values[$field_name])) {
        $config->set('episodes.image_field_mappings.' . $field_name, $values[$field_name]);
      }
    }

    if (isset($values['show_reference'])) {
      $config->set('episodes.references.show', $values['show_reference']);
    }
    if (isset($values['season_reference'])) {
      $config->set('episodes.references.season', $values['season_reference']);
    }
    if (isset($values['episode_type_vocabularly'])) {
      $config->set('episodes.references.episode_type_vocabularly', $values['episode_type_vocabularly']);
    }

    $config->save();

    parent::submitForm($form, $form_state);
  }

}
