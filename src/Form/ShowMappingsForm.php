<?php

namespace Drupal\pbs_media_manager\Form;

use Drupal\Core\Config\ConfigFactory;
use Drupal\Core\Datetime\DateFormatterInterface;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\pbs_media_manager\ApiClient;
use Drupal\pbs_media_manager\ShowManager;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class ShowMappingsForm.
 *
 * @ingroup pbs_media_manager
 */
class ShowMappingsForm extends ConfigFormBase {

  /**
   * Media Manager API client.
   *
   * @var \Drupal\pbs_media_manager\ApiClient
   */
  protected $apiClient;

  /**
   * Date formatting service.
   *
   * @var \Drupal\Core\Datetime\DateFormatterInterface
   */
  protected $dateFormatter;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The entity field manager.
   *
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  protected $entityFieldManager;

  /**
   * Constructs a new MediaManagerSettingsForm.
   *
   * @param \Drupal\Core\Config\ConfigFactory $config_factory
   *   Config factory service.
   * @param \Drupal\pbs_media_manager\ApiClient $api_client
   *   Media Manager API client service.
   * @param \Drupal\Core\Datetime\DateFormatterInterface $date_formatter
   *   Date formatter service.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Entity\EntityFieldManagerInterface $entity_field_manager
   *   The entity field manager.
   */
  public function __construct(
    ConfigFactory $config_factory,
    ApiClient $api_client,
    DateFormatterInterface $date_formatter,
    EntityTypeManagerInterface $entity_type_manager,
    EntityFieldManagerInterface $entity_field_manager
  ) {
    parent::__construct($config_factory);
    $this->apiClient = $api_client;
    $this->dateFormatter = $date_formatter;
    $this->entityTypeManager = $entity_type_manager;
    $this->entityFieldManager = $entity_field_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('pbs_media_manager.api_client'),
      $container->get('date.formatter'),
      $container->get('entity_type.manager'),
      $container->get('entity_field.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'pbs_media_manager_show_mappings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames(): array {
    return ['pbs_media_manager.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $config = $this->config('pbs_media_manager.settings');
    $show_content_type = $config->get('shows.drupal_show_content');

    $form['shows_queue'] = [
      '#type' => 'details',
      '#title' => $this->t('Show queue settings'),
      '#open' => TRUE,
    ];

    $form['shows_queue']['shows_queue_enable'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable automated queue building'),
      '#description' => $this->t('Enable incremental updates to local Show
        nodes from Media Manager data.'),
      '#default_value' => $config
        ->get(ShowManager::getAutoUpdateConfigName()),
      '#return_value' => TRUE,
    ];

    $interval_options = [1, 900, 3600, 10800, 21600, 43200, 86400, 604800];
    $form['shows_queue']['shows_queue_interval'] = [
      '#type' => 'select',
      '#title' => $this->t('Queue builder update interval'),
      '#description' => $this->t('How often to check Media Manager for
        new or updated shows to add to the queue. The queue itself is processed
        one every cron ron (or by an external cron operation). This interval
        will also be used for Seasons and Episodes, if configured.'),
      '#default_value' => $config
        ->get(ShowManager::getAutoUpdateIntervalConfigName()),
      '#options' => array_map(
        [$this->dateFormatter, 'formatInterval'],
        array_combine($interval_options, $interval_options)
      ),
      '#states' => [
        'visible' => [
          'input[name="shows_queue_enable"]' => ['checked' => TRUE],
        ],
      ],
    ];

    $form['show_vocab'] = [
      '#type' => 'details',
      '#title' => $this->t('Show vocabulary'),
      '#open' => TRUE,
      '#states' => [
        'visible' => [
          'input[name="shows_queue_enable"]' => ['checked' => TRUE],
        ],
      ],
    ];
    $show_vocab = $config->get('shows.show_vocabulary');
    $vocabs = array_keys($this->entityTypeManager->getStorage('taxonomy_vocabulary')->loadMultiple());
    $vocabulary_options = array_combine($vocabs, $vocabs) + ['unused' => 'unused'];
    $form['show_vocab']['show_vocabulary'] = [
      '#type' => 'select',
      '#title' => $this->t('Drupal show vocabulary'),
      '#description' => $this->t('The vocabulary will contain the show IDS (PBS Content IDs) to keep synced with this site. In addition to name, this vocabulary must include a plain text field with the machine name field_show_guid and the formatter for that field must be set to "PBS Media Manager show guid convert to show name". If "unused" is selected all available shows will be synced.'),
      '#default_value' => $show_vocab,
      '#options' => $vocabulary_options,
    ];
    $items = [];
    if (!empty($show_vocab) && $show_vocab != 'unused') {

      // Add show link.
      $options = ['absolute' => TRUE];
      $request = $this->getRequest();
      $url = Url::fromRoute('entity.taxonomy_term.add_form', [
        'taxonomy_vocabulary' => $show_vocab,
        'destination' => $request->getRequestUri(),
      ], $options);
      $form['show_vocab']['add_show'] = [
        '#type' => 'link',
        '#title' => $this->t('Add show taxonomy term'),
        '#url' => $url,
        '#attributes' => [
          'class' => ['button'],
        ],
      ];

      // Subscribed shows.
      $taxonomy_manager = $this->entityTypeManager->getStorage('taxonomy_term');
      $subscribed_shows = $taxonomy_manager->loadByProperties([
        'vid' => $show_vocab,
      ]);
      foreach ($subscribed_shows as $show) {
        $show_name = $show->getName();
        $show_guid = $show->get('field_show_guid')->value;
        $items[] = $this->t('@name (@guid)', [
          '@name' => $show_name,
          '@guid' => $show_guid,
        ]);
      }
      $form['show_vocab']['subscriptions'] = [
        '#type' => 'markup',
        '#theme' => 'item_list',
        '#title' => $this->t('Subscribed Shows'),
        '#items' => $items,
      ];
    }

    $form['show_content_type'] = [
      '#type' => 'details',
      '#title' => $this->t('Show content type'),
      '#open' => FALSE,
      '#states' => [
        'visible' => [
          'input[name="shows_queue_enable"]' => ['checked' => TRUE],
        ],
      ],
    ];
    $drupal_content_types = array_keys($this->entityTypeManager->getStorage('node_type')->loadMultiple());
    $content_type_options = array_combine($drupal_content_types, $drupal_content_types);
    $form['show_content_type']['drupal_show_content'] = [
      '#type' => 'select',
      '#title' => $this->t('Drupal show content type'),
      '#description' => $this->t('Select the Drupal content type to use for Media Manager show content.'),
      '#default_value' => $config->get('shows.drupal_show_content'),
      '#options' => $content_type_options,
    ];

    $form['show_field_mappings'] = [
      '#type' => 'details',
      '#title' => $this->t('Show field mappings'),
      '#open' => FALSE,
      '#states' => [
        'visible' => [
          'input[name="shows_queue_enable"]' => ['checked' => TRUE],
        ],
      ],
    ];
    if (!empty($config->get('shows.drupal_show_content'))) {
      $required_show_fields = [
        'title',
        'id',
        'updated_at',
        'tms_id',
      ];
      $show_fields = array_keys($this->entityFieldManager
        ->getFieldDefinitions('node', $show_content_type));
      $show_field_options = ['unused' => 'unused'] +
        array_combine($show_fields, $show_fields);
      $pbs_show_fields = $config->get('shows.mappings');
      foreach (array_keys($pbs_show_fields) as $field_name) {
        $required = in_array($field_name, $required_show_fields) ? TRUE : FALSE;
        $form['show_field_mappings'][$field_name] = [
          '#type' => 'select',
          '#title' => $field_name,
          '#options' => $show_field_options,
          '#required' => $required,
          '#default_value' => $pbs_show_fields[$field_name],
        ];
      }
    }
    else {
      $form['show_field_mappings']['mappings_required'] = [
        '#type' => 'item',
        '#markup' => 'Select and save Drupal show content type (above) to choose field mappings.',
      ];
    }

    // Image media type configuration.
    $form['image_settings'] = [
      '#type' => 'details',
      '#title' => $this->t('Image settings'),
      '#open' => FALSE,
      '#states' => [
        'visible' => [
          'input[name="shows_queue_enable"]' => ['checked' => TRUE],
        ],
      ],
    ];
    $media_types = array_keys($this->entityTypeManager->getStorage('media_type')->loadMultiple());
    $media_type_options = array_combine($media_types, $media_types);
    $image_media_type = $config->get('shows.image_media_type');
    $form['image_settings']['image_media_type'] = [
      '#type' => 'select',
      '#title' => $this->t('Drupal image media type'),
      '#default_value' => $image_media_type,
      '#options' => $media_type_options,
    ];
    // Media image field mappings.
    if (!empty($image_media_type)) {
      $form['image_settings']['image_field_mappings'] = [
        '#type' => 'details',
        '#title' => $this->t('Image field mappings'),
        '#open' => TRUE,
      ];
      $image_media_fields = array_keys($this
        ->entityFieldManager
        ->getFieldDefinitions('media', $image_media_type));
      $image_field_options = ['unused' => 'unused'] +
        array_combine($image_media_fields, $image_media_fields);
      $pbs_image_fields = $config->get('shows.image_field_mappings');
      foreach ($pbs_image_fields as $pbs_image_field => $field_value) {
        $form['image_settings']['image_field_mappings'][$pbs_image_field] = [
          '#type' => 'select',
          '#title' => $pbs_image_field,
          '#options' => $image_field_options,
          '#default_value' => $pbs_image_fields[$pbs_image_field],
          '#required' => TRUE,
        ];
      }
    }
    else {
      $form['image_settings']['image_field_mappings']['mappings_required'] = [
        '#type' => 'item',
        '#markup' => 'Select and save the Drupal image media type to choose field mappings.',
      ];
    }

    $form['vocabularies'] = [
      '#type' => 'details',
      '#title' => $this->t('Genre vocabulary'),
      '#open' => FALSE,
      '#states' => [
        'visible' => [
          'input[name="shows_queue_enable"]' => ['checked' => TRUE],
        ],
      ],
    ];
    $form['vocabularies']['genre_vocabulary'] = [
      '#type' => 'select',
      '#title' => 'Genre vocabulary (taxonomy)',
      '#description' => $this->t('Select the vocabulary to use for the show genre field.'),
      '#options' => $vocabulary_options,
      '#default_value' => $config->get('shows.genre.genre_vocabulary'),
    ];

    $genre_vocab = $config->get('shows.genre.genre_vocabulary');
    if ($genre_vocab != '' &&
      $genre_fields = array_keys($this
        ->entityFieldManager
        ->getFieldDefinitions('taxonomy_term', $genre_vocab))
    ) {
      $vocab_fields = ['unused' => 'unused'] + array_combine($genre_fields, $genre_fields);
      $form['vocabularies']['genre_title'] = [
        '#type' => 'select',
        '#title' => 'Genre title field',
        '#description' => $this->t('Select the title field of the genre vocabulary (probably "name").'),
        '#options' => $vocab_fields,
        '#default_value' => $config->get('shows.genre.genre_title'),
      ];
      $form['vocabularies']['genre_id'] = [
        '#type' => 'select',
        '#title' => 'Genre ID field',
        '#description' => $this->t('Select the ID field of the genre vocabulary.'),
        '#options' => $vocab_fields,
        '#default_value' => $config->get('shows.genre.genre_id'),
      ];
      $form['vocabularies']['genre_slug'] = [
        '#type' => 'select',
        '#title' => 'Genre slug field',
        '#description' => $this->t('Select the slug field of the genre vocabulary.'),
        '#options' => $vocab_fields,
        '#default_value' => $config->get('shows.genre.genre_slug'),
      ];
    }
    else {
      $form['vocabularies']['genre_vocabulary_required'] = [
        '#type' => 'item',
        '#markup' => 'Select and save a genre vocabulary to choose genre vocabulary field mappings.',
      ];
    }

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();
    $config = $this->config('pbs_media_manager.settings');

    $config->set('shows.show_vocabulary', $values['show_vocabulary']);
    $config->set('shows.drupal_show_content', $values['drupal_show_content']);

    // Queue settings.
    $config->set(
      ShowManager::getAutoUpdateConfigName(),
      $values['shows_queue_enable']
    );
    if ($values['shows_queue_enable']) {
      $config->set(
        ShowManager::getAutoUpdateIntervalConfigName(),
        (int) $values['shows_queue_interval']
      );
    }

    // Show field mappings.
    $pbs_show_fields = $config->get('shows.mappings');
    foreach (array_keys($pbs_show_fields) as $field_name) {
      if (isset($values[$field_name])) {
        $config->set('shows.mappings.' . $field_name, $values[$field_name]);
      }
    }

    // Image settings.
    $config->set('shows.image_media_type', $values['image_media_type']);
    $pbs_image_fields = $config->get('shows.image_field_mappings');
    foreach ($pbs_image_fields as $field_name => $field_value) {
      if (isset($values[$field_name])) {
        $config->set('shows.image_field_mappings.' . $field_name, $values[$field_name]);
      }
    }

    // Genre vocabulary and fields.
    $config->set('shows.genre.genre_vocabulary', $values['genre_vocabulary']);
    if (!empty($values['genre_title'])) {
      $config->set('shows.genre.genre_title', $values['genre_title']);
    }
    if (!empty($values['genre_id'])) {
      $config->set('shows.genre.genre_id', $values['genre_id']);
    }
    if (!empty($values['genre_slug'])) {
      $config->set('shows.genre.genre_slug', $values['genre_slug']);
    }

    $config->save();

    parent::submitForm($form, $form_state);
  }

}
