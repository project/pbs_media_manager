<?php

namespace Drupal\pbs_media_manager\Form;

use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class VideoMappingsForm.
 *
 * @ingroup pbs_media_manager
 */
class VideoMappingsForm extends ConfigFormBase {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The entity field manager.
   *
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  protected $entityFieldManager;

  /**
   * Constructs a new VideoMappingsForm.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Entity\EntityFieldManagerInterface $entity_field_manager
   *   The entity field manager.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, EntityFieldManagerInterface $entity_field_manager) {
    $this->entityTypeManager = $entity_type_manager;
    $this->entityFieldManager = $entity_field_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('entity_field.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'pbs_media_manager_video_mappings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames(): array {
    return ['pbs_media_manager.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $config = $this->config('pbs_media_manager.settings');

    $media_types = array_keys($this->entityTypeManager->getStorage('media_type')->loadMultiple());
    $media_type_options = array_combine($media_types, $media_types);
    $video_media_type = $config->get('videos.video_media_type');
    $form['video_settings']['video_media_type'] = [
      '#type' => 'select',
      '#title' => $this->t('Drupal video media type'),
      '#default_value' => $video_media_type,
      '#options' => $media_type_options,
    ];

    // Media video field mappings.
    if (!empty($video_media_type)) {
      $form['video_settings']['mappings'] = [
        '#type' => 'details',
        '#title' => $this->t('Video field mappings'),
        '#open' => FALSE,
      ];
      $video_media_fields = array_keys($this
        ->entityFieldManager
        ->getFieldDefinitions('media', $video_media_type));
      $video_field_options = ['unused' => 'unused'] +
        array_combine($video_media_fields, $video_media_fields);
      $required_video_media_fields = [
        'id',
        'title',
        'updated_at',
      ];
      if ($pbs_video_fields = $config->get('videos.mappings')) {
        foreach ($pbs_video_fields as $pbs_video_field => $field_value) {
          $required = in_array($pbs_video_field, $required_video_media_fields) ? TRUE : FALSE;
          $form['video_settings']['mappings'][$pbs_video_field] = [
            '#type' => 'select',
            '#title' => $pbs_video_field,
            '#options' => $video_field_options,
            '#required' => $required,
            '#default_value' => $pbs_video_fields[$pbs_video_field],
          ];
        }
      }
    }
    else {
      $form['video_settings']['mappings']['mappings_required'] = [
        '#type' => 'item',
        '#markup' => 'Select and save the Drupal video media type to choose field mappings.',
      ];
    }

    // Image media type configuration.
    $form['image_settings'] = [
      '#type' => 'details',
      '#title' => $this->t('Image settings'),
      '#open' => FALSE,
    ];
    $media_types = array_keys($this->entityTypeManager->getStorage('media_type')->loadMultiple());
    $media_type_options = array_combine($media_types, $media_types);
    $image_media_type = $config->get('shows.image_media_type');
    $form['image_settings']['image_media_type'] = [
      '#type' => 'select',
      '#title' => $this->t('Drupal image media type'),
      '#default_value' => $image_media_type,
      '#options' => $media_type_options,
    ];
    // Media image field mappings.
    if (!empty($image_media_type)) {
      $form['image_settings']['image_field_mappings'] = [
        '#type' => 'details',
        '#title' => $this->t('Image field mappings'),
        '#open' => TRUE,
      ];
      $image_media_fields = array_keys($this
        ->entityFieldManager
        ->getFieldDefinitions('media', $image_media_type));
      $image_field_options = ['unused' => 'unused'] +
        array_combine($image_media_fields, $image_media_fields);
      $pbs_image_fields = $config->get('shows.image_field_mappings');
      foreach ($pbs_image_fields as $pbs_image_field => $field_value) {
        $form['image_settings']['image_field_mappings'][$pbs_image_field] = [
          '#type' => 'select',
          '#title' => $pbs_image_field,
          '#options' => $image_field_options,
          '#default_value' => $pbs_image_fields[$pbs_image_field],
          '#required' => TRUE,
        ];
      }
    }
    else {
      $form['image_settings']['image_field_mappings']['mappings_required'] = [
        '#type' => 'item',
        '#markup' => 'Select and save the Drupal image media type to choose field mappings.',
      ];
    }


    // Video references.
    // media video field mappings.
    if (!empty($video_media_type)) {
      $form['video_references'] = [
        '#type' => 'details',
        '#title' => $this->t('Video references'),
        '#open' => FALSE,
      ];
      if (!empty($config->get('videos.video_media_type'))) {
        $form['video_references']['show_reference'] = [
          '#type' => 'select',
          '#title' => 'Show reference field',
          '#options' => $video_field_options,
          '#default_value' => $config->get('videos.references.show'),
        ];
        $form['video_references']['season_reference'] = [
          '#type' => 'select',
          '#title' => 'Season reference field',
          '#options' => $video_field_options,
          '#default_value' => $config->get('videos.references.season'),
        ];
        $form['video_references']['episode_reference'] = [
          '#type' => 'select',
          '#title' => 'Episode reference field',
          '#options' => $video_field_options,
          '#default_value' => $config->get('videos.references.episode'),
        ];
      }
    }

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();
    $config = $this->config('pbs_media_manager.settings');

    if ($pbs_video_fields = $config->get('videos.mappings')) {
      foreach (array_keys($pbs_video_fields) as $field) {
        if (!empty($values[$field])) {
          $config->set('videos.mappings.' . $field, $values[$field]);
        }
      }
    }

    // Video settings.
    $config->set('videos.video_media_type', $values['video_media_type']);
    $pbs_video_fields = $config->get('videos.mappings');
    foreach ($pbs_video_fields as $field_name => $field_value) {
      if (isset($values[$field_name])) {
        $config->set('videos.mappings.' . $field_name, $values[$field_name]);
      }
    }

    if (isset($values['show_reference'])) {
      $config->set('videos.references.show', $values['show_reference']);
    }
    if (isset($values['season_reference'])) {
      $config->set('videos.references.season', $values['season_reference']);
    }
    if (isset($values['episode_reference'])) {
      $config->set('videos.references.episode', $values['episode_reference']);
    }
    $config->save();

    parent::submitForm($form, $form_state);
  }

}
