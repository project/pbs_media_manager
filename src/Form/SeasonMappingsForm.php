<?php

namespace Drupal\pbs_media_manager\Form;

use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class SeasonMappingsForm.
 *
 * @ingroup pbs_media_manager
 */
class SeasonMappingsForm extends ConfigFormBase {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The entity field manager.
   *
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  protected $entityFieldManager;

  /**
   * Constructs a new SeasonMappingsForm.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Entity\EntityFieldManagerInterface $entity_field_manager
   *   The entity field manager.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, EntityFieldManagerInterface $entity_field_manager) {
    $this->entityTypeManager = $entity_type_manager;
    $this->entityFieldManager = $entity_field_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('entity_field.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'pbs_media_manager_season_mappings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames(): array {
    return ['pbs_media_manager.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $config = $this->config('pbs_media_manager.settings');
    $season_content_type = $config->get('seasons.drupal_season_content');

    $form['season_sync_enable'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable incremental updates to local Season nodes
        from Media Manager data.'),
      '#default_value' => $config->get('seasons.season_sync_enable'),
      '#return_value' => TRUE,
    ];

    $form['seasons'] = [
      '#type' => 'details',
      '#title' => $this->t('Season content type'),
      '#open' => TRUE,
      '#states' => [
        'visible' => [
          'input[name="season_sync_enable"]' => ['checked' => TRUE],
        ],
      ],
    ];

    // Season content type.
    $drupal_content_types = array_keys($this->entityTypeManager->getStorage('node_type')->loadMultiple());
    $content_type_options = array_combine($drupal_content_types, $drupal_content_types);
    $form['seasons']['drupal_season_content'] = [
      '#type' => 'select',
      '#title' => $this->t('Drupal season content type'),
      '#description' => $this->t('Select the Drupal content type to use for Media Manager season content.'),
      '#default_value' => $config->get('seasons.drupal_season_content'),
      '#options' => $content_type_options,
    ];

    // Season mappings.
    $form['season_field_mappings'] = [
      '#type' => 'details',
      '#title' => $this->t('Season Media Manager field mappings'),
      '#open' => TRUE,
      '#states' => [
        'visible' => [
          'input[name="season_sync_enable"]' => ['checked' => TRUE],
        ],
      ],
    ];
    $required_season_fields = [
      'id',
      'updated_at',
    ];
    if (!empty($config->get('seasons.drupal_season_content'))) {
      $season_fields = array_keys($this->entityFieldManager
        ->getFieldDefinitions('node', $season_content_type));
      $season_field_options = ['unused' => 'unused'] +
        array_combine($season_fields, $season_fields);
      $pbs_season_fields = $config->get('seasons.mappings');
      foreach (array_keys($pbs_season_fields) as $field_name) {
        $required = in_array($field_name, $required_season_fields) ? TRUE : FALSE;
        $form['season_field_mappings'][$field_name] = [
          '#type' => 'select',
          '#title' => $field_name,
          '#options' => $season_field_options,
          '#required' => $required,
          '#default_value' => $pbs_season_fields[$field_name],
        ];
      }
    }
    else {
      $form['season_field_mappings']['mappings_required'] = [
        '#type' => 'item',
        '#markup' => 'Select and save Drupal season content type (above) to choose field mappings.',
      ];
    }

    // Season references.
    $form['season_references'] = [
      '#type' => 'details',
      '#title' => $this->t('Season references'),
      '#open' => TRUE,
      '#states' => [
        'visible' => [
          'input[name="season_sync_enable"]' => ['checked' => TRUE],
        ],
      ],
    ];
    if (!empty($config->get('seasons.drupal_season_content'))) {
      $form['season_references']['show_reference'] = [
        '#type' => 'select',
        '#title' => 'Show reference field',
        '#options' => $season_field_options,
        '#default_value' => $config->get('seasons.references.show'),
      ];
    }

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();
    $config = $this->config('pbs_media_manager.settings');

    $config->set('seasons.season_sync_enable', $values['season_sync_enable']);
    $config->set('seasons.drupal_season_content', $values['drupal_season_content']);

    // Save data for all mapped fields.
    $pbs_season_fields = $config->get('seasons.mappings');
    foreach (array_keys($pbs_season_fields) as $field) {
      if (!empty($values[$field])) {
        $config->set('seasons.mappings.' . $field, $values[$field]);
      }
    }

    if (isset($values['show_reference'])) {
      $config->set('seasons.references.show', $values['show_reference']);
    }
    $config->save();

    parent::submitForm($form, $form_state);
  }

}
