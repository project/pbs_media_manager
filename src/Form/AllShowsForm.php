<?php

namespace Drupal\pbs_media_manager\Form;

use Drupal\Core\Database\Connection;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\pbs_media_manager\ApiClient;
use OpenPublicMedia\PbsMediaManager\Client;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Form to display all available shows.
 */
class AllShowsForm extends FormBase {

  /**
   * The PBS Media Manager client.
   *
   * @var \Drupal\pbs_media_manager\ApiClient
   */
  protected $client;

  /**
   * The database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $connection;

  /**
   * Constructs the AllShowsForm.
   *
   * @param \Drupal\pbs_media_manager\ApiClient $api_client
   *   The Media Manager API service.
   * @param \Drupal\Core\Database\Connection $connection
   *   The database connection.
   */
  public function __construct(ApiClient $api_client, Connection $connection) {
    $this->client = $api_client;
    $this->connection = $connection;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('pbs_media_manager.api_client'),
      $container->get('database')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'pbs_media_manager_shows';
  }

  /**
   * Display a list of all PBS shows available.
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $form['rebuild_show_list'] = [
      '#type' => 'submit',
      '#value' => $this->t('Rebuild the show list'),
    ];

    $config = $this->config('pbs_media_manager.settings');
    $vocab = $config->get('shows.show_vocabulary');
    if (!empty($vocab) && $vocab != 'unused') {
      $options = ['absolute' => TRUE];
      $request = $this->getRequest();
      $url = Url::fromRoute('entity.taxonomy_term.add_form', [
        'taxonomy_vocabulary' => $vocab,
        'destination' => $request->getRequestUri(),
      ], $options);
      $form['add_show'] = [
        '#type' => 'link',
        '#title' => $this->t('Add show taxonomy term'),
        '#url' => $url,
        '#attributes' => [
          'class' => ['button'],
        ],
      ];
    }

    // Retrieve the current list of shows.
    $results = $this->connection
      ->query("SELECT * FROM {pbs_media_manager_shows}")
      ->fetchAll();

    if (!empty($results)) {
      foreach ($results as $result) {
        $show_rows[] = [
          $result->show_name,
          $result->guid,
          $result->episode_count,
        ];
      }

      $form['show_count'] = [
        '#type' => 'item',
        '#markup' => $this->t('Show count: @count', ['@count' => count($show_rows)]),
      ];

    }
    else {
      $show_rows[] = ['Please rebuild the show list.'];
    }

    $header = [
      $this->t('Show'),
      $this->t('GUID'),
      $this->t('Episode Count'),
    ];

    // Sort the shows.
    array_multisort($show_rows, SORT_ASC);

    $form['show_list'] = [
      '#theme' => 'table',
      '#header' => $header,
      '#rows' => $show_rows,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    // Empty the list of shows in the database.
    $this->connection->truncate('pbs_media_manager_shows')->execute();

    // The maximum allowed number of shows that can be retrieved in one API
    // call is 50. Determine how many API calls are necessary.
    $shows = $this->client->getShows();
    $pagedResponse = $shows->getResponse();
    $show_count = $pagedResponse->getTotalItemsCount();
    $total_pages = $pagedResponse->count();
    $pages = range(1, $total_pages);

    // Setup one batch operation per API call.
    $operations = [];
    foreach ($pages as $page) {
      $operations[] = ['\Drupal\pbs_media_manager\Form\AllShowsForm::addShows', [$page]];
    }

    // Define the batch.
    $batch = [
      'title' => $this->t('Rebuilding the list with @count shows from PBS...', ['@count' => $show_count]),
      'progress_message' => $this->t('Processed @current out of @total batches of shows.'),
      'error_message' => $this->t('An error occurred during processing'),
      'operations' => $operations,
      'finished' => '\Drupal\pbs_media_manager\Form\AllShowsForm::batchFinished',
    ];
    batch_set($batch);

  }

  /**
   * Add one "page" of PBS shows.
   */
  public static function addShows($page, &$context) {

    // Use the $context['sandbox'] at your convenience to store the
    // information needed to track progression between successive calls.
    if (empty($context['sandbox'])) {
      $context['sandbox'] = [];
      $context['sandbox']['progress'] = 0;
    }

    // Callback functions cannot be called from an object context:
    // https://api.drupal.org/api/drupal/core%21includes%21form.inc/group/batch/9.0.x
    // Therefore, procedural code must be used in this static method.
    $context['sandbox']['progress']++;
    $context['sandbox']['current_page'] = $page;

    /** @var \Drupal\Core\Config\ConfigFactoryInterface $config_factory */
    $config_factory = \Drupal::service('config.factory');
    $config = $config_factory->get('pbs_media_manager.settings');
    $key = $config->get('api.key');
    $secret = $config->get('api.secret');
    /** @var OpenPublicMedia\PbsMediaManager\Client client */
    $client = new Client($key, $secret);

    // Process the (up to) 50 shows from the page.
    $shows = $client->getShows(['page' => $page]);
    $pagedResponse = $shows->getResponse();
    foreach ($pagedResponse->current()->data as $show) {
      \Drupal::database()->insert('pbs_media_manager_shows')
        ->fields([
          'show_name' => $show->attributes->title,
          'guid' => $show->id,
          'episode_count' => $show->attributes->episodes_count ?: 0,
        ])
        ->execute();

    }

    $context['results'][] = $page;
  }

  /**
   * Display success or error message.
   */
  public static function batchFinished($success, $results, $operations) {
    if ($success) {
      $message = 'List completed with ' . count($results) . ' batches of shows.';
    }
    else {
      $message = 'Finished with an error.';
    }
    \Drupal::messenger()->addMessage($message);
  }

}
