<?php

namespace Drupal\pbs_media_manager\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\node\NodeInterface;
use Drupal\pbs_media_manager\EpisodeManager;
use Drupal\pbs_media_manager\SeasonManager;
use Drupal\pbs_media_manager\ShowManager;
use Drupal\pbs_media_manager\SpecialManager;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class ShowUpdateBlockForm.
 */
class ShowUpdateBlockForm extends FormBase {

  /**
   * The PBS Media Manager show manager.
   *
   * @var \Drupal\pbs_media_manager\ShowManager
   */
  protected $showManager;

  /**
   * The PBS Media Manager season manager.
   *
   * @var \Drupal\pbs_media_manager\SeasonManager
   */
  protected $seasonManager;

  /**
   * The PBS Media Manager episode manager.
   *
   * @var \Drupal\pbs_media_manager\EpisodeManager
   */
  protected $episodeManager;

  /**
   * The PBS Media Manager special manager.
   *
   * @var \Drupal\pbs_media_manager\SpecialManager
   */
  protected $specialManager;

  /**
   * The messenger service.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * Constructs a new DrushCommands object.
   *
   * @param \Drupal\pbs_media_manager\ShowManager $show_manager
   *   The PBS Media Manager show manager.
   * @param \Drupal\pbs_media_manager\SeasonManager $season_manager
   *   The PBS Media Manager season manager.
   * @param \Drupal\pbs_media_manager\EpisodeManager $episode_manager
   *   The PBS Media Manager episode manager.
   * @param \Drupal\pbs_media_manager\SpecialManager $special_manager
   *   The PBS Media Manager special manager.
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   The messenger service.
   */
  public function __construct(ShowManager $show_manager, SeasonManager $season_manager, EpisodeManager $episode_manager, SpecialManager $special_manager, MessengerInterface $messenger) {
    $this->showManager = $show_manager;
    $this->seasonManager = $season_manager;
    $this->episodeManager = $episode_manager;
    $this->specialManager = $special_manager;
    $this->messenger = $messenger;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('pbs_media_manager.show_manager'),
      $container->get('pbs_media_manager.season_manager'),
      $container->get('pbs_media_manager.episode_manager'),
      $container->get('pbs_media_manager.special_manager'),
      $container->get('messenger')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'show_update_block_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['update'] = [
      '#type' => 'submit',
      '#value' => $this->t('Update Show'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $node = $this->getRouteMatch()->getParameter('node');

    $config = $this->config('pbs_media_manager.settings');
    $show_id_field = $config->get('shows.mappings.id');
    if (empty($show_id_field)) {
      $this->messenger->addWarning($this->t('The "id" value in the "Show field
        mappings" must be configured to update this show.'));
      return;
    }

    if ($node instanceof NodeInterface) {
      $uuid = $node->get($show_id_field)->value;
      $title = $node->getTitle();
    }

    if (empty($uuid)) {
      $this->messenger->addWarning($this->t('The Media Manager ID field is empty for the show "@title".', ['@title' => $title]));
      return;
    }

    // First, update the show.
    $show = $this->showManager->getShow($uuid);
    if (!is_object($show)) {
      $this->messenger->addWarning($this->t('The show "@title" was not found in Media Manager.', ['@title' => $title]));
      return;
    }
    $this->showManager->addOrUpdateShow($show, TRUE);
    $this->messenger->addStatus(
      $this->t('The show "@title" has been updated.', [
        '@title' => $title,
      ]
    ));
    // Second, if the show has seasons, update all of them.
    if ($seasons = $show->attributes->seasons) {
      foreach ($seasons as $season_item) {
        $season = $this->seasonManager->getSeason($season_item->id);
        if (is_object($season)) {
          $this->seasonManager->addOrUpdateSeason($season, TRUE);
          $this->messenger->addStatus(
            $this->t('Season @ordinal has been updated.', [
              '@ordinal' => $season->attributes->ordinal,
            ]
          ));
        }

        // Third, update all of the episodes for each season.
        if ($episodes = $season->attributes->episodes) {
          foreach ($episodes as $episode_item) {
            $episode = $this->episodeManager->getEpisode($episode_item->id);
            if (is_object($episode)) {
              $this->episodeManager->addOrUpdateEpisode($episode, TRUE);
              $this->messenger->addStatus(
                $this->t('Episode "@title" has been updated.', [
                  '@title' => $episode->attributes->title,
                ]
              ));
            }
          }
        }
      }
    }
    // Fourth, if there are specials (episodes), add those.
    $client = $this->episodeManager->getApiClient();
    if ($specials = $client->getSpecials($uuid)) {
      foreach ($specials as $special) {
        if ($special_item = $client->getSpecial($special->id)) {
          $this->specialManager->addOrUpdateSpecial($special_item, TRUE);
          $this->messenger->addStatus(
            $this->t('Special "@title" has been updated.', [
              '@title' => $special_item->attributes->title,
            ]
          ));
        }
      }
    }
  }

}
