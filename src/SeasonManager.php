<?php

namespace Drupal\pbs_media_manager;

use DateTime;
use Drupal\datetime\Plugin\Field\FieldType\DateTimeItemInterface;
use Exception;
use OpenPublicMedia\PbsMediaManager\Exception\BadRequestException;
use stdClass;

/**
 * Class SeasonManager.
 *
 * @package Drupal\pbs_media_manager
 */
class SeasonManager extends ApiContentManagerBase {

  /**
   * State key for the last update DateTime.
   */
  const LAST_UPDATE_KEY = 'pbs_media_manager.shows.last_update';

  /**
   * {@inheritdoc}
   */
  public static function getEntityTypeId(): string {
    return 'node';
  }

  /**
   * {@inheritdoc}
   */
  public function getBundleId(): string {
    return $this->config->get('seasons.drupal_season_content');
  }

  /**
   * {@inheritdoc}
   */
  public static function getQueueName(): string {
    return 'pbs_media_manager.queue.seasons';
  }

  /**
   * {@inheritdoc}
   */
  public function getLastUpdateTime(): DateTime {
    return $this->state->get(
      self::LAST_UPDATE_KEY,
      new DateTime('@1')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function setLastUpdateTime(DateTime $time): void {
    $this->state->set(self::LAST_UPDATE_KEY, $time);
  }

  /**
   * {@inheritdoc}
   */
  public function resetLastUpdateTime(): void {
    $this->state->delete(self::LAST_UPDATE_KEY);
  }

  /**
   * {@inheritdoc}
   */
  public static function getAutoUpdateConfigName(): string {
    return 'season.queue.autoupdate';
  }

  /**
   * {@inheritdoc}
   */
  public static function getAutoUpdateIntervalConfigName(): string {
    return 'seasons.queue.autoupdate_interval';
  }

  /**
   * {@inheritdoc}
   */
  public function updateQueue(DateTime $since = NULL): bool {
    $dt_start = new DateTime();
    $logging = $this->config->get('logging.operations');

    // Initialize counter for tracking items added to queue.
    $count = 0;

    $show_vocabulary = $this->config->get('shows.show_vocabulary');
    if (!empty($show_vocabulary) && $show_vocabulary != 'unused') {
      $taxonomy_manager = $this->entityTypeManager->getStorage('taxonomy_term');
      $subscribed_shows = $taxonomy_manager->loadByProperties([
        'vid' => $show_vocabulary,
      ]);
      foreach ($subscribed_shows as $show) {
        $show_guid = $show->get('field_show_guid')->value;
        // Initialize counter for tracking items added to the queue per show.
        $count_per_show = 0;
        try {
          $seasons = $this->client->getSeasons($show_guid);
        }
        catch (BadRequestException $e) {
          // Only log show errors in ShowManager.
        }
        if (is_object($seasons)) {
          foreach ($seasons as $season) {
            if ($season_item = $this->client->getSeason($season->id)) {
              $this->getQueue()->createItem($season_item);
              $count++;
              $count_per_show++;
            }
          }
        }
        if ($count_per_show && !$logging['queue']) {
          $this->logger->debug('{count} seasons have been added to the queue for show {show}.', [
            'count' => $count_per_show,
            'show' => $show->getName()
          ]);
        }
      }
    }
    else {
      $shows = $this->client->getShows(['fetch-related' => FALSE]);
      foreach ($shows as $show) {
        // Initialize counter for tracking items added to the queue per show.
        $count_per_show = 0;
        try {
          $seasons = $this->client->getSeasons($show->id);
        }
        catch (\Exception $e) {
          // If we don't have access to the season, just skip it.
          continue;
        }
        foreach ($seasons as $season) {
          if ($season_item = $this->client->getSeason($season->id)) {
            $this->getQueue()->createItem($season_item);
            $count++;
            $count_per_show++;
          }
        }
        if ($count_per_show && !$logging['queue']) {
          $this->logger->debug('{count} seasons have been added to the queue for show {show}.', [
            'count' => $count_per_show,
            'show' => $show->attributes->title
          ]);
        }
      }
    }

    if (!$logging['queue']) {
      $this->logger->notice('{count} total seasons have been added to the queue.', ['count' => $count,]);
    }


    $this->setLastUpdateTime($dt_start);

    return TRUE;
  }

  /**
   * Gets an API response for a single season.
   *
   * @param string $guid
   *   ID of the season to get.
   *
   * @return object|null
   *   season item data from the API or NULL.
   */
  public function getSeason(string $guid): ?stdClass {
    return $this->client->getSeason($guid);
  }

  /**
   * Get season nodes with optional properties.
   *
   * @param array $properties
   *   Properties to filter season nodes.
   *
   * @return \Drupal\node\NodeInterface[]
   *   seasons nodes.
   */
  public function getSeasonNodes(array $properties = []): array {
    try {
      $definition = $this->entityTypeManager->getDefinition(self::getEntityTypeId());
      $storage = $this->entityTypeManager->getStorage(self::getEntityTypeId());
      $nodes = $storage->loadByProperties([
        $definition->getKey('bundle') => $this->getBundleId(),
      ] + $properties);
    }
    catch (Exception $e) {
      // Let NULL fall through.
      $nodes = [];
    }

    return $nodes;
  }

  /**
   * Adds or updates a season node based on API data.
   *
   * @param object $item
   *   An API response object for the season.
   * @param bool $force
   *   Whether or not to "force" the update. If FALSE (default) and a current
   *   season content node is found with a matching or newer "last_updated"
   *   value, the existing node will not be updated. If TRUE, "last_updated" is
   *   ignored.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityStorageException
   * @throws \Exception
   */
  public function addOrUpdateSeason(object $item, $force = FALSE): void {

    $mappings = $this->config->get('seasons.mappings');
    $node = $this->getOrCreateNode($item->id, $this->getBundleId(), 'seasons');

    $operation = $this->getOperation($item, $node, $mappings, $force);
    $logging = $this->config->get('logging.operations');

    if ($operation == 'configured') {
      $this->logger->error($this->t('Please map the title, id, and updated_at fields for seasons.'));
      return;
    }
    if ($operation == 'skipped') {
      if (!$logging['skipped']) {
        $this->logger->debug($this->t('The season @title has not changed', [
          '@title' => $node->getTitle(),
        ]));
      }
      return;
    }

    // We need to save the node before creating the videos because seasons
    // reference videos and videos reference seasons. Therefore, we also have
    // to set the title of the season. Additionally, we need the guid because
    // addOrUpdateAssetContent() looks up nodes by guid.
    $attributes = $item->attributes;
    $season_number = $attributes->ordinal ?: '';
    $show_title = $attributes->show->attributes->title ?: '';
    if (!empty($attributes->title)) {
      $season_title = $attributes->title;
    }
    elseif (!empty($season_number) && !empty($show_title)) {
      $season_title = $show_title . ": Season " . $season_number;
    }
    else {
      $this->logger->error($this->t('The season ID @id is missing a title, season number, and/or associated show title.', [
        '@id' => $item->id,
      ]));
      return;
    }
    $node->setTitle($season_title);
    if (!empty($item->id) && $mappings['id'] != 'unused') {
      $node->set($mappings['id'], $item->id);
    }
    $node->save();

    // Regardless of whether or not the season was updated or not, also check
    // the assests to determine if they have been updated.
    if ($video_field = $mappings['video']) {
      if (!empty($video_field) && $video_field !== 'unused' && !empty($attributes->assets)) {
        $videos = $this->client->get('seasons/' . $item->id . "/assets");
        $video_drupal_ids = [];
        foreach ($videos as $video) {
          if ($media_video = $this->addOrUpdateAssetContent($video, $force)) {
            $video_drupal_ids[] = ['target_id' => $media_video->id()];
          }
        }
        $node->set($video_field, $video_drupal_ids);
      }
    }

    if ($operation == 'ignored') {
      if (!$logging['ignored']) {
        $this->logger->debug($this->t('The season @id was ignored due to a hook_pbs_media_manager_operation_alter() implementation.', [
          '@id' => $item->id,
        ]));
      }
      return;
    }

    if (!empty($item->id) && $mappings['id'] != 'unused') {
      $node->set($mappings['id'], $item->id);
    }
    if (!empty($attributes->ordinal) && $mappings['ordinal'] != 'unused') {
      $node->set($mappings['ordinal'], $attributes->ordinal);
    }
    if (!empty($attributes->description_long) &&
      $mappings['description_long'] != 'unused') {
      $node->set($mappings['description_long'], $attributes->description_long);
    }
    if (!empty($attributes->description_short) &&
      $mappings['description_short'] != 'unused') {
      $node->set($mappings['description_short'], $attributes->description_short);
    }

    $updated_at = self::getLatestUpdatedAt($item);
    $node->set(
      $mappings['updated_at'],
      $updated_at->format(DateTimeItemInterface::DATETIME_STORAGE_FORMAT)
    );

    // Add the show reference, if configured.
    $show_ref_field = $this->config->get('seasons.references.show');
    $show_guid = $attributes->show->id;
    $show_bundle = $this->config->get('shows.drupal_show_content');
    if (!empty($show_guid) && !empty($show_bundle) && $show_ref_field != 'unused') {
      $show_node = $this->getNodeByGuid($show_guid, $show_bundle, 'shows');
      if (!empty($show_node) && !empty($show_node->id())) {
        $node->set($show_ref_field, ['target_id' => $show_node->id()]);
      }
    }

    $node->save();

    if (!empty($node->getTitle()) & !empty($operation && !$logging['changed'])) {
      $this->logger->notice($this->t('Season @title has been @operation', [
        '@title' => $node->getTitle(),
        '@operation' => $operation,
      ]));
    }
  }

}
