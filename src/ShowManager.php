<?php

namespace Drupal\pbs_media_manager;

use DateTime;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Entity\EntityInterface;
use Drupal\datetime\Plugin\Field\FieldType\DateTimeItemInterface;
use Drupal\node\NodeInterface;
use Drupal\taxonomy\Entity\Term;
use Exception;
use stdClass;

/**
 * Class ShowManager.
 *
 * @package Drupal\pbs_media_manager
 */
class ShowManager extends ApiContentManagerBase {

  /**
   * State key for the last update DateTime.
   */
  const LAST_UPDATE_KEY = 'pbs_media_manager.shows.last_update';

  /**
   * {@inheritdoc}
   */
  public static function getEntityTypeId(): string {
    return 'node';
  }

  /**
   * {@inheritdoc}
   */
  public function getBundleId(): string {
    return $this->config->get('shows.drupal_show_content');
  }

  /**
   * {@inheritdoc}
   */
  public static function getQueueName(): string {
    return 'pbs_media_manager.queue.shows';
  }

  /**
   * {@inheritdoc}
   */
  public function getLastUpdateTime(): DateTime {
    return $this->state->get(
      self::LAST_UPDATE_KEY,
      new DateTime('@1')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function setLastUpdateTime(DateTime $time): void {
    $this->state->set(self::LAST_UPDATE_KEY, $time);
  }

  /**
   * {@inheritdoc}
   */
  public function resetLastUpdateTime(): void {
    $this->state->delete(self::LAST_UPDATE_KEY);
  }

  /**
   * {@inheritdoc}
   */
  public static function getAutoUpdateConfigName(): string {
    return 'shows.queue.autoupdate';
  }

  /**
   * {@inheritdoc}
   */
  public static function getAutoUpdateIntervalConfigName(): string {
    return 'shows.queue.autoupdate_interval';
  }

  /**
   * Gets all existing Genre entities from the database.
   *
   * @return array
   *   Genre entities keyed by Media Manager ID.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function getGenres(): array {
    $definition = $this->entityTypeManager->getDefinition('taxonomy_term');
    $storage = $this->entityTypeManager->getStorage('taxonomy_term');
    $genre_vocab = $this->config->get('shows.genre.genre_vocabulary');
    $genre_id_field = $this->config->get('shows.genre.genre_id');
    /** @var \Drupal\taxonomy\Entity\Term[] $entities */
    $entities = $storage->loadByProperties([
      $definition->getKey('bundle') => $genre_vocab,
    ]);
    // Re-key by Media Manager ID.
    $genres = [];
    foreach ($entities as $entity) {
      $genres[$entity->get($genre_id_field)->value] = $entity;
    }
    return $genres;
  }

  /**
   * Gets an existing Genre term (creating a new one if necessary).
   *
   * This method also adds the newly created genre to the self::genres array so
   * it can be reused with further objects processed by this class.
   *
   * @param string $id
   *   Media Manager ID for the Genre.
   * @param string $name
   *   Genre name.
   * @param string $slug
   *   Genre slug.
   *
   * @return \Drupal\Core\Entity\EntityInterface
   *   A Genre term.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  private function getOrAddGenre(string $id, string $name, string $slug): EntityInterface {
    $config = $this->config;
    $genres = $this->getGenres();
    if (isset($genres[$id])) {
      $genre = $genres[$id];
    }
    else {
      $definition = $this->entityTypeManager->getDefinition('taxonomy_term');
      $genre = Term::create([
        $definition->getKey('bundle') => $config->get('shows.genre.genre_vocabulary'),
      ]);
      $genre->setName($name);
      $genre_id_field = $this->config->get('shows.genre.genre_id');
      if (!empty($genre_id_field) && $genre_id_field != 'unused') {
        $genre->set($genre_id_field, $id);
      }
      $genre_slug_field = $this->config->get('shows.genre.genre_slug');
      if (!empty($genre_slug_field) && $genre_slug_field != 'unused') {
        $genre->set($genre_slug_field, $slug);
      }
      $genre->save();
    }
    return $genre;
  }

  /**
   * {@inheritdoc}
   *
   * @throws \Exception
   */
  public function updateQueue(DateTime $since = NULL): bool {
    $dt_start = new DateTime();
    $logging = $this->config->get('logging.operations');

    // Initialize counter for tracking items added to queue.
    $count = 0;

    $show_vocabulary = $this->config->get('shows.show_vocabulary');
    if (!empty($show_vocabulary) && $show_vocabulary != 'unused') {
      $taxonomy_manager = $this->entityTypeManager->getStorage('taxonomy_term');
      $subscribed_shows = $taxonomy_manager->loadByProperties([
        'vid' => $show_vocabulary,
      ]);
      foreach ($subscribed_shows as $show) {
        $show_guid = $show->get('field_show_guid')->value;
        $show_item = $this->client->getShow($show_guid);
        if (is_object($show_item)) {
          $this->getQueue()->createItem($show_item);
          $count++;
          // Give feedback for every 50 shows.
          if ($count % 50 === 0 && !$logging['queue']) {
            $this->logger->debug('{count} shows have been added to the queue.',
              ['count' => $count ]);
          }
        }
        else {
          $this->logger->error($this->t('The show @title (@id) did not return any results.', [
            '@title' => $show->getName(),
            '@id' => $show_guid,
          ]));
        }
      }
    }
    else {
      // Get all shows available. This query cannot sort by update time.
      // Results must be fully traversed.
      $shows = $this->client->getShows(['fetch-related' => TRUE]);
      foreach ($shows as $show) {
        $updated_at = self::getLatestUpdatedAt($show);
        if ($updated_at > $since) {
          $this->getQueue()->createItem($show);
          $count++;
          // Give feedback for every 50 shows.
          if ($count % 50 === 0 && !$logging['queue']) {
            $this->logger->debug('{count} shows have been added to the queue.',
              ['count' => $count ]);
          }
        }
      }
    }

    if (!$logging['queue']) {
      $this->logger->notice('{count} total shows have been added to the queue.', ['count' => $count ]);
    }


    $this->setLastUpdateTime($dt_start);

    return TRUE;
  }

  /**
   * Gets an API response for a single Show.
   *
   * @param string $guid
   *   ID of the Show to get.
   *
   * @return object|null
   *   Show item data from the API or NULL.
   */
  public function getShow(string $guid): ?stdClass {
    return $this->client->getShow($guid);
  }

  /**
   * Get Show nodes with optional properties.
   *
   * @param array $properties
   *   Properties to filter Show nodes.
   *
   * @return \Drupal\node\NodeInterface[]
   *   Shows nodes.
   */
  public function getShowNodes(array $properties = []): array {
    try {
      $definition = $this->entityTypeManager->getDefinition(self::getEntityTypeId());
      $storage = $this->entityTypeManager->getStorage(self::getEntityTypeId());
      $nodes = $storage->loadByProperties([
        $definition->getKey('bundle') => $this->getBundleId(),
      ] + $properties);
    }
    catch (Exception $e) {
      // Let NULL fall through.
      $nodes = [];
    }

    return $nodes;
  }

  /**
   * Gets a Show node by TMS ID.
   *
   * @param string $id
   *   TMS ID to query with.
   *
   * @return \Drupal\node\NodeInterface|null
   *   Show node with TMS ID or NULL if none found.
   */
  public function getShowNodeByTmsId(string $id): ?NodeInterface {
    if ($tms_id_field = $this->config->get('shows.mappings.tms_id')) {
      $nodes = $this->getShowNodes([$tms_id_field => $id]);

      $node = NULL;
      if (!empty($nodes)) {
        $node = reset($nodes);
        if (count($nodes) > 1) {
          $this->logger->error('Multiple nodes found for Media Manager
            TMS ID {id}. Node IDs found: {nid_list}. Using node {nid}.', [
              'id' => $id,
              'nid_list' => implode(', ', array_keys($nodes)),
              'nid' => $node->id(),
            ]);
        }
      }
    }
    else {
      $this->logger->error('Mapping the show TMS ID field is required.');
    }
    return $node;
  }

  /**
   * Gets a Show node by slug.
   *
   * @param string $slug
   *   Slug to query with.
   *
   * @return \Drupal\node\NodeInterface|null
   *   Show node with the slug or NULL if none found.
   */
  public function getShowNodeBySlug(string $slug): ?NodeInterface {
    $nodes = $this->getShowNodes(['field_show_slug' => $slug]);

    $node = NULL;
    if (!empty($nodes)) {
      $node = reset($nodes);
    }

    return $node;
  }

  /**
   * Adds or updates a Show node based on API data.
   *
   * @param object $item
   *   An API response object for the Show.
   * @param bool $force
   *   Whether or not to "force" the update. If FALSE (default) and a current
   *   show content node is found with a matching or newer "last_updated"
   *   value, the existing node will not be updated. If TRUE, "last_updated" is
   *   ignored.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityStorageException
   * @throws \Exception
   */
  public function addOrUpdateShow(object $item, $force = FALSE): void {

    $mappings = $this->config->get('shows.mappings');
    $node = $this->getOrCreateNode($item->id, $this->getBundleId(), 'shows');
    $operation = $this->getOperation($item, $node, $mappings, $force);
    $logging = $this->config->get('logging.operations');

    // Do not proceed if mappings are missing.
    if ($operation == 'configured') {
      $this->logger->error($this->t('Please map the title, id, and updated_at fields for shows.'));
      return;
    }
    // Do not consider videos for ignored shows.
    if ($operation == 'ignored') {
      if (!$logging['ignored']) {
        $this->logger->debug($this->t('The show @id was ignored due to a hook_pbs_media_manager_operation_alter() implementation.', [
          '@id' => $item->id,
        ]));
      }

      return;
    }

    // We need to save the node before creating the videos because shows
    // reference videos and videos reference shows. Therefore, we also have
    // to set the title of the show. Additionally, we need the guid because
    // addOrUpdateAssetContent() looks up nodes by guid.
    $attributes = $item->attributes;
    if (empty($attributes->title)) {
      return;
    }
    $node->setTitle($attributes->title);
    if (!empty($item->id) && $mappings['id'] != 'unused') {
      $node->set($mappings['id'], $item->id);
    }
    $node->save();

    // Regardless of whether or not the show was updated or not, also check
    // the assets to determine if they have been updated.
    if ($video_field = $mappings['video']) {
      if (!empty($video_field) && $video_field !== 'unused' && !empty($attributes->assets)) {
        $videos = [];
        try {
          $videos = $this->client->get('shows/' . $item->id . "/assets");
        }
        catch (Exception $e) {
          $this->logger->error(t('The show ID @id asset request failed. Msg: @msg', [
            '@id' => $item->id,
            '@msg' => $e->getMessage(),
          ]));
        }
        $video_drupal_ids = [];
        foreach ($videos as $video) {
          if ($media_video = $this->addOrUpdateAssetContent($video, $force)) {
            $video_drupal_ids[] = ['target_id' => $media_video->id()];
          }
        }
        $node->set($video_field, $video_drupal_ids);
      }
    }

    if ($operation == 'skipped') {
      if (!$logging['skipped']) {
        $this->logger->debug($this->t('The show @title has not changed', [
          '@title' => $node->getTitle(),
        ]));
      }
      return;
    }

    // Get the call sign field and value.
    $call_sign_field = $mappings['call_sign'];
    $stations = array_column($attributes->audience, 'station');
    $call_sign = '';
    foreach ($stations as $station) {
      if (isset($station->type) && $station->type == 'station') {
        $call_sign = $station->attributes->call_sign;
      }
    }

    if (!empty($call_sign_field) && $mappings['call_sign'] != 'unused') {
      $node->set($call_sign_field, $call_sign);
    }
    if (!empty($item->id) && $mappings['id'] != 'unused') {
      $node->set($mappings['id'], $item->id);
    }
    if (!empty($attributes->tms_id) && $mappings['tms_id'] != 'unused') {
      $node->set($mappings['tms_id'], $attributes->tms_id);
    }
    if (!empty($attributes->description_long) &&
        $mappings['description_long'] != 'unused') {
      $node->set($mappings['description_long'], $attributes->description_long);
    }
    if (!empty($attributes->description_short) &&
        $mappings['description_short'] != 'unused') {
      $node->set($mappings['description_short'], $attributes->description_short);
    }
    if (!empty($attributes->nola) &&
        $mappings['nola'] != 'unused') {
      $node->set($mappings['nola'], $attributes->nola);
    }

    if (!empty($attributes->drm_enabled) && $mappings['drm_enabled'] != 'unused') {
      $node->set($mappings['drm_enabled'], $attributes->drm_enabled);
    }

    $updated_at = self::getLatestUpdatedAt($item);
    $node->set(
      $mappings['updated_at'],
      $updated_at->format(DateTimeItemInterface::DATETIME_STORAGE_FORMAT)
    );

    // Add the genre, if mapped.
    if (!empty($attributes->genre) && $genre_field = $mappings['genre']) {
      if ($genre_field != 'unused') {
        $node->set($genre_field, $this->getOrAddGenre(
          $attributes->genre->id,
          $attributes->genre->title,
          $attributes->genre->slug
        ));
      }
    }

    // Logic to handle the links if the Social Media Links Block and Field
    // module (https://www.drupal.org/project/social_media_links) is enabled.
    // TODO: Add additional options to handle the links array.
    $links_field = $mappings['links'];
    if (!empty($attributes->links) &&
      $links_field != 'unused' &&
      $this->moduleHandler->moduleExists('social_media_links_field')) {
      foreach ($attributes->links as $link) {
        if (isset($node->{$links_field}->platform_values)) {
          $link_value = $link->value;
          // All profiles except newsletters & email have a hard coded base URL.
          if (!in_array($link->profile, ['newsletter', 'email'])) {
            $link_value = parse_url($link->value, PHP_URL_PATH);
            // Remove beginning and trailing slashed.
            $link_value = preg_replace('/^\//', '', $link_value);
            $link_value = preg_replace('/\/$/', '', $link_value);
          }
          $platform_values[$link->profile]['value'] = $link_value;
        }
      }
      $node->set($links_field, ['platform_values' => $platform_values]);
    }

    // Process images.
    if ($images = $attributes->images) {
      foreach ($images as $image) {
        if (!empty($mappings[$image->profile]) && $image_field = $mappings[$image->profile]) {
          if ($image_field != 'unused') {
            // Check to see if this item has an image already.
            $media_image = NULL;
            if ($node->get($image_field)->target_id) {
              // Get the existing image entity.
              $image_id = $node->get($image_field)->target_id;
              $media_storage = $this->entityTypeManager->getStorage('media');
              $media_image = $media_storage->load($image_id);
            }
            if ($media_id = $this->addOrUpdateMediaImage(
              $image,
              $attributes->title . ": " . $image->profile,
              'show',
              $media_image
            )) {
              $node->set($image_field, ['target_id' => $media_id]);
            }
          }
        }
      }
    }

    $node->save();

    if (!empty($node->getTitle()) & !empty($operation && !$logging['changed'])) {
      $this->logger->notice($this->t('Show @title has been @operation', [
        '@title' => $node->getTitle(),
        '@operation' => $operation,
      ]));
    }
  }

  /**
   * Gets IDs of available (published) Video Content nodes related to a Show.
   *
   * @param \Drupal\node\NodeInterface $show
   *   Show node.
   * @param bool $include_scheduled
   *   Whether to include include Video Content scheduled in the future (via the
   *   contrib module Scheduler).
   *
   * @return array
   *   Node IDs of related Video Content nodes.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function getVideoContent(
    NodeInterface $show,
    bool $include_scheduled = FALSE
  ): array {
    $definition = $this->entityTypeManager
      ->getDefinition(VideoContentManager::getEntityTypeId());
    $query = $this->entityTypeManager
      ->getStorage(VideoContentManager::getEntityTypeId())
      ->getQuery();

    $query
      ->condition($definition->getKey('bundle'), VideoContentManager::getBundleId())
      ->condition('field_show_ref', $show->id());

    if ($include_scheduled) {
      $now = new DrupalDateTime();
      $or = $query->orConditionGroup();
      $or->condition('status', 1);
      $or->condition('publish_on', $now->getTimestamp(), '>');
      $query->condition($or);
    }
    else {
      $query->condition('status', 1);
    }

    return $query->execute();
  }

  /**
   * Determines if a Show is suitable for publishing consideration.
   *
   * Note: This method does not determine whether or not a Show should be
   * published. It determines if the Show should be _considered_ for publishing.
   *
   * Currently, the only requirement for a Show to be publishable is that it
   * does not have an audience scope of "kids".
   *
   * @param \Drupal\node\NodeInterface $show
   *   Show node.
   *
   * @return bool
   *   TRUE if the Show is publishable, FALSE otherwise.
   */
  public static function showIsPublishable(NodeInterface $show): bool {
    $field = 'field_audience_scope';
    if ($show->hasField($field) && !$show->get($field)->isEmpty()
      && $show->get($field)->value === 'kids') {
      return FALSE;
    }
    return TRUE;
  }

}
