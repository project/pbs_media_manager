<?php

namespace Drupal\pbs_media_manager;

use DateTime;
use Drupal\datetime\Plugin\Field\FieldType\DateTimeItemInterface;
use Drupal\taxonomy\Entity\Term;
use stdClass;

/**
 * Class SpecialManager.
 *
 * Treat Specials almost exactly as Episodes, with a few small differences.
 *
 * @package Drupal\pbs_media_manager
 */
class SpecialManager extends EpisodeManager {

  /**
   * {@inheritdoc}
   *
   * @throws \Exception
   */
  public function updateQueue(DateTime $since = NULL): bool {
    $dt_start = new DateTime();
    $logging = $this->config->get('logging.operations');

    // Initialize counter for tracking items added to queue.
    $count = 0;

    $show_vocabulary = $this->config->get('shows.show_vocabulary');
    if (!empty($show_vocabulary) && $show_vocabulary != 'unused') {
      $taxonomy_manager = $this->entityTypeManager->getStorage('taxonomy_term');
      $subscribed_shows = $taxonomy_manager->loadByProperties([
        'vid' => $show_vocabulary,
      ]);
      foreach ($subscribed_shows as $show) {
        $show_guid = $show->get('field_show_guid')->value;
        // Initialize counter for tracking items added to the queue per show.
        $count_per_show = 0;
        try {
          $specials = $this->client->getSpecials($show_guid);
        }
        catch (\Exception $e) {
          // If we don't have access to the special, just skip it.
          continue;
        }
        foreach ($specials as $special) {
          try {
            $special_item = $this->client->getSpecial($special->id);
            $this->getQueue()->createItem($special_item);
            $count++;
            $count_per_show++;
            // Give an incremental update every 50 items to avoid timeouts for a show with many specials.
            if ($count_per_show % 50 === 0 && !$logging['queue']) {
              $this->logger->debug('{show} update: {count} specials added to the queue.', [
                'count' => $count_per_show,
                'show' => $show->getName()
              ]);
            }
          }
          catch (\Exception $e) {
            // If we don't have access to the special, just skip it.
            continue;
          }
        }
        if (!$logging['queue']) {
          $this->logger->debug('{count} specials have been added to the queue for show {show}.', [
            'count' => $count_per_show,
            'show' => $show->getName()
          ]);
        }

      }
    }
    else {
      $shows = $this->client->getShows(['fetch-related' => FALSE]);
      foreach ($shows as $show) {
        // Initialize counter for tracking items added to the queue per show.
        $count_per_show = 0;
        try {
          $specials = $this->client->getSpecials($show->id);
        }
        catch (\Exception $e) {
          // If we don't have access to the special, just skip it.
          continue;
        }
        if (is_object($specials)) {
          foreach ($specials as $special) {
            if ($special_item = $this->client->getSpecial($special->id)) {
              $this->getQueue()->createItem($special_item);
              $count++;
              $count_per_show++;
              // Give an incremental update every 50 items to avoid timeouts for a show with many specials.
              if ($count_per_show % 50 === 0 && !$logging['queue']) {
                $this->logger->debug('{show} update: {count} specials added to the queue.', [
                  'count' => $count_per_show,
                  'show' => $show->attributes->title
                ]);
              }
            }
          }
          if ($count_per_show && !$logging['queue']) {
            $this->logger->debug('{count} specials have been added to the queue for show {show}.', [
              'count' => $count_per_show,
              'show' => $show->attributes->title
            ]);
          }
        }
      }
    }

    if (!$logging['queue']) {
      $this->logger->notice('{count} total specials have been added to the queue.', ['count' => $count,]);
    }


    $this->setLastUpdateTime($dt_start);

    return TRUE;
  }

  /**
   * Adds or updates a Special node based on API data.
   *
   * @param object $item
   *   An API response object for the Special.
   * @param bool $force
   *   Whether or not to "force" the update. If FALSE (default) and a current
   *   special content node is found with a matching or newer "last_updated"
   *   value, the existing node will not be updated. If TRUE, "last_updated" is
   *   ignored.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityStorageException
   * @throws \Exception
   */
  public function addOrUpdateSpecial(object $item, $force = FALSE): void {

    // Treat specials like episodes.
    $node = $this->getOrCreateNode($item->id, $this->getBundleId(), 'episodes');
    $mappings = $this->config->get('episodes.mappings');
    $operation = $this->getOperation($item, $node, $mappings, $force);
    $logging = $this->config->get('logging.operations');

    if ($operation == 'configured') {
      $this->logger->error($this->t('Please map the title, id, and updated_at fields for specials.'));
      return;
    }
    // Do not consider videos for ignored specials.
    if ($operation == 'ignored') {
      if (!$logging['ignored']) {
        $this->logger->debug($this->t('The special @id was ignored due to a hook_pbs_media_manager_operation_alter() implementation.', [
          '@id' => $item->id,
        ]));
      }
      return;
    }

    // We need to save the node before creating the videos because specials
    // reference videos and videos reference specials. Therefore, we also have
    // to set the title of the special. Additionally, we will also need the
    // guid because addOrUpdateAssetContent() looks up nodes by guid.
    $attributes = $item->attributes;
    if (empty($attributes->title)) {
      return;
    }
    $node->setTitle($attributes->title);
    if (!empty($item->id) && $mappings['id'] != 'unused') {
      $node->set($mappings['id'], $item->id);
    }
    $node->save();

    // Regardless of whether or not the special was updated or not, also check
    // the assets to determine if they have been updated.
    if ($video_field = $mappings['video']) {
      if (!empty($video_field) && $video_field !== 'unused' && !empty($attributes->assets)) {
        $videos = $this->client->get('specials/' . $item->id . "/assets");
        $video_drupal_ids = [];
        foreach ($videos as $video) {
          if ($media_video = $this->addOrUpdateAssetContent($video, $force)) {
            $video_drupal_ids[] = ['target_id' => $media_video->id()];
          }
        }
        $node->set($video_field, $video_drupal_ids);
        // Even if the Special has not changed, we added or updated videos, so
        // save those to the Special.
        // TODO: Determine the operation performed and only save the special if
        // it has changed.
        $node->save();
      }
    }

    if ($operation == 'skipped') {
      if (!$logging['skipped']) {
        $this->logger->debug($this->t('The special @title has not changed', [
          '@title' => $node->getTitle(),
        ]));
      }
      return;
    }

    $type = $mappings['type'];
    $episode_type_vocab = $this->config->get('episodes.references.episode_type_vocabularly');
    if (!empty($item->type) && !empty($type) && $type != 'unused') {
      $taxonomy_storage = $this->entityTypeManager->getStorage('taxonomy_term');
      if ($term = $taxonomy_storage->loadByProperties([
        'name' => $item->type,
        'vid' => $episode_type_vocab,
      ])) {
        $term = reset($term);
      }
      else {
        $term = Term::create([
          'name' => $item->type,
          'vid' => $episode_type_vocab,
        ]);
        $term->save();
      }
      $node->set($type, ['target_id' => $term->id()]);
    }
    // Most of these are the same as episodes, except no ordinal.
    $other_attributes = [
      'tms_id',
      'description_long',
      'description_short',
      'nola',
      'premiered_on',
      'encored_on',
      'slug',
      'language'
    ];
    foreach ($other_attributes as $attribute) {
      $attribute_field = $mappings[$attribute];
      if (!empty($attributes->{$attribute}) && !empty($attribute_field) && $attribute_field != 'unused') {
        $node->set($attribute_field, $attributes->{$attribute});
      }
    }

    $updated_at = self::getLatestUpdatedAt($item);
    if ($mappings['updated_at'] != 'unused') {
      $node->set(
        $mappings['updated_at'],
        $updated_at->format(DateTimeItemInterface::DATETIME_STORAGE_FORMAT)
      );
    }

    // Process images.
    if (!empty($attributes->assets)) {
      foreach($attributes->assets as $asset) {
        // There can be multiple "related" assets.
        // First check for a full length asset, then fall back to the first
        // image available.
        if (!empty($asset->attributes->images)) {
          if (isset($asset->attributes->object_type) && $asset->attributes->object_type == 'full_length') {
            $images = $asset->attributes->images;
            break;
          }
          elseif (empty($images)) {
            $images = $asset->attributes->images;
          }
        }
      }
    }

    if (isset($images) && is_array($images)) {
      foreach ($images as $image) {
        if (!empty($mappings[$image->profile]) && $image_field = $mappings[$image->profile]) {
          if ($image_field != 'unused') {
            // Again, treat these like episode images.
            // Check to see if this item has an image already.
            $media_image = NULL;
            if ($node->get($image_field)->target_id) {
              // Get the existing image entity.
              $image_id = $node->get($image_field)->target_id;
              $media_storage = $this->entityTypeManager->getStorage('media');
              $media_image = $media_storage->load($image_id);
            }
            if ($media_id = $this->addOrUpdateMediaImage(
              $image,
              $attributes->title . ": " . $image->profile,
              'episode',
              $media_image
            )) {
              $node->set($image_field, ['target_id' => $media_id]);
            }
          }
        }
      }
    }

    // Add the show reference, if configured. No season references for specials.
    $show_ref_field = $this->config->get('episodes.references.show');
    $show_guid = $attributes->show->id;
    $show_bundle = $this->config->get('shows.drupal_show_content');
    if (!empty($show_guid) && !empty($show_bundle) && $show_ref_field != 'unused') {
      $show_node = $this->getNodeByGuid($show_guid, $show_bundle, 'shows');
      if (!empty($show_node) && !empty($show_node->id())) {
        $node->set($show_ref_field, ['target_id' => $show_node->id()]);
      }
    }

    // Save the node.
    $node->save();

    if (!empty($node->getTitle()) & !empty($operation && !$logging['changed'])) {
      $this->logger->notice($this->t('Special @title has been @operation', [
        '@title' => $node->getTitle(),
        '@operation' => $operation,
      ]));
    }
  }

  /**
   * Gets an API response for a single Special.
   *
   * @param string $guid
   *   ID of the Special to get.
   *
   * @return object|null
   *   Special item data from the API or NULL.
   */
  public function getSpecial(string $guid): ?stdClass {
    return $this->client->getSpecial($guid);
  }

}
