<?php

namespace Drupal\pbs_media_manager\Commands;

use Drupal\Core\Queue\QueueFactory;
use Drupal\Core\Queue\QueueWorkerManagerInterface;
use Drupal\Core\Queue\RequeueException;
use Drupal\Core\Queue\SuspendQueueException;
use Drupal\pbs_media_manager\EpisodeManager;
use Drupal\pbs_media_manager\SeasonManager;
use Drupal\pbs_media_manager\ShowManager;
use Drupal\pbs_media_manager\SpecialManager;
use Drush\Commands\DrushCommands;

/**
 * A Drush commandfile.
 */
class PbsCommands extends DrushCommands {

  /**
   * The PBS Media Manager show manager.
   *
   * @var \Drupal\pbs_media_manager\ShowManager
   */
  protected $showManager;

  /**
   * The PBS Media Manager season manager.
   *
   * @var \Drupal\pbs_media_manager\SeasonManager
   */
  protected $seasonManager;

  /**
   * The PBS Media Manager episode manager.
   *
   * @var \Drupal\pbs_media_manager\EpisodeManager
   */
  protected $episodeManager;

  /**
   * The PBS Media Manager special manager.
   *
   * @var \Drupal\pbs_media_manager\SpecialManager
   */
  protected $specialManager;

  /**
   * The queue worker manager.
   *
   * @var \Drupal\Core\Queue\QueueWorkerManagerInterface
   */
  protected $workerManager;

  /**
   * The queue service.
   *
   * @var \Drupal\Core\Queue\QueueFactory
   */
  protected $queueService;

  /**
   * Constructs a new DrushCommands object.
   *
   * @param \Drupal\pbs_media_manager\ShowManager $show_manager
   *   The PBS Media Manager show manager.
   * @param \Drupal\pbs_media_manager\SeasonManager $season_manager
   *   The PBS Media Manager season manager.
   * @param \Drupal\pbs_media_manager\EpisodeManager $episode_manager
   *   The PBS Media Manager episode manager.
   * @param \Drupal\pbs_media_manager\SpecialManager $special_manager
   *   The PBS Media Manager special manager.
   * @param \Drupal\Core\Queue\QueueWorkerManagerInterface $worker_manager
   *   The queue worker manager.
   * @param \Drupal\Core\Queue\QueueFactory $queue_service
   *   The queue service.
   */
  public function __construct(ShowManager $show_manager, SeasonManager $season_manager, EpisodeManager $episode_manager, SpecialManager $special_manager, QueueWorkerManagerInterface $worker_manager, QueueFactory $queue_service) {
    $this->showManager = $show_manager;
    $this->seasonManager = $season_manager;
    $this->episodeManager = $episode_manager;
    $this->specialManager = $special_manager;
    $this->workerManager = $worker_manager;
    $this->queueService = $queue_service;
  }

  /**
   * Command to pull a single Media Manager show.
   *
   * @param string $show_id
   *   The Media Manager id of the show.
   * @param array $options
   *   Associative array of options.
   *
   * @command pbs_media_manager:getShow
   * @aliases pbs-gs
   */
  public function getShow($show_id, array $options = ['force' => FALSE]) {
    $show = $this->showManager->getShow($show_id);
    if (is_object($show)) {
      $this->showManager->addOrUpdateShow($show, $options['force']);
    }
    else {
      $this->logger->error(dt('The show ID @id did not return any results.', [
        '@id' => $show_id,
      ]));
    }
  }

  /**
   * Command to pull a single Media Manager season.
   *
   * @param string $season_id
   *   The Media Manager id of the season.
   * @param array $options
   *   Associative array of options.
   *
   * @command pbs_media_manager:getSeason
   * @aliases pbs-gn
   */
  public function getSeason($season_id, array $options = ['force' => FALSE]) {
    $season = $this->seasonManager->getSeason($season_id);
    if (is_object($season)) {
      $this->seasonManager->addOrUpdateSeason($season, $options['force']);
    }
    else {
      $this->logger->error(dt('The season ID @id did not return any results.', [
        '@id' => $season_id,
      ]));
    }
  }

  /**
   * Command to pull a single Media Manager episode.
   *
   * @param string $episode_id
   *   The Media Manager id of the episode.
   * @param array $options
   *   Associative array of options.
   *
   * @usage drush pbs-gv 745a2571-05fd-449e-93b6-0b8fb89e6fa1 --force
   *   Import the episode or force it to update even if it has not changed.
   *
   * @command pbs_media_manager:getEpisode
   * @aliases pbs-ge
   */
  public function getEpisode($episode_id, array $options = ['force' => FALSE]) {
    $episode = $this->episodeManager->getEpisode($episode_id);
    if (is_object($episode)) {
      $this->episodeManager->addOrUpdateEpisode($episode, $options['force']);
    }
    else {
      $this->logger->error(dt('The episode ID @id did not return any results.', [
        '@id' => $episode_id,
      ]));
    }
  }

  /**
   * Command to pull a single Media Manager special.
   *
   * @param string $special_id
   *   The Media Manager id of the special.
   * @param array $options
   *   Associative array of options.
   *
   * @usage drush pbs-gp 398e358e-42d1-4c01-b91f-0ceb48568b16 --force
   *   Import the special or force it to update even if it has not changed.
   *
   * @command pbs_media_manager:getSpecial
   * @aliases pbs-gp
   */
  public function getSpecial($special_id, array $options = ['force' => FALSE]) {
    $special = $this->specialManager->getSpecial($special_id);
    if (is_object($special)) {
      $this->specialManager->addOrUpdateSpecial($special, $options['force']);
    }
    else {
      $this->logger->error(dt('The special ID @id did not return any results.', [
        '@id' => $special_id,
      ]));
    }
  }

  /**
   * Command to pull a single Media Manager asset.
   *
   * @param string $asset_id
   *   The Media Manager id of the asset.
   * @param array $options
   *   Associative array of options.
   *
   * @usage drush pbs-gv 82bb45da-12de-4a52-8ff9-5ee02dd30442 --force
   *   Import the asset or force it to update even if it has not changed.
   *
   * @command pbs_media_manager:getAsset
   * @aliases pbs-ga
   */
  public function getAsset($asset_id, array $options = ['force' => FALSE]) {
    // This sort of hijacks the EpisodeManager class.
    $client = $this->episodeManager->getApiClient();
    $asset = $client->getAsset($asset_id);
    if (is_object($asset)) {
      $this->episodeManager->addOrUpdateAssetContent($asset, $options['force']);
    }
    else {
      $this->logger->error(dt('The asset ID @id did not return any results.', [
        '@id' => $asset_id,
      ]));
    }
  }

  /**
   * Command to add all (subscribed) shows to the Drupal queue.
   *
   * @command pbs_media_manager:getAllShows
   * @aliases pbs-gas
   */
  public function getAllShows() {
    $this->showManager->updateQueue();
    $this->output()->writeln(dt('Process the shows with `drush queue:run pbs_media_manager.queue.shows` or run cron.'));
  }

  /**
   * Command to add all (subscribed) seasons to the Drupal queue.
   *
   * @command pbs_media_manager:getAllSeasons
   * @aliases pbs-gan
   */
  public function getAllSeasons() {
    $this->seasonManager->updateQueue();
    $this->output()->writeln(dt('Process the seasons with `drush queue:run pbs_media_manager.queue.seasons` or run cron.'));
  }

  /**
   * Command to add all (subscribed) episodes to the Drupal queue.
   *
   * @command pbs_media_manager:getAllEpisodes
   * @aliases pbs-gae
   */
  public function getAllEpisodes() {
    $this->episodeManager->updateQueue();
    $this->output()->writeln(dt("Process the episodes with `drush queue:run pbs_media_manager.queue.episodes` or run cron."));
  }

  /**
   * Command to add all (subscribed) specials to the Drupal queue.
   *
   * @command pbs_media_manager:getAllSpecials
   * @aliases pbs-gap
   */
  public function getAllSpecials() {
    $this->specialManager->updateQueue();
    $this->output()->writeln(dt("Specials are stored in the episode queue. Process the specials with `drush queue:run pbs_media_manager.queue.episodes` or run cron."));
  }

  /**
   * Command to get all content for a single show.
   *
   * @param string $show_id
   *   The Media Manager id of the show.
   * @param array $options
   *   Associative array of options.
   *
   * @command pbs_media_manager:getShowAll
   * @aliases pbs-gsa
   */
  public function getShowAll($show_id, array $options = ['force' => FALSE]) {
    $this->getShow($show_id, $options);
    $client = $this->showManager->getApiClient();
    try {
      $seasons = $client->getSeasons($show_id);
    }
    catch (BadRequestException $e) {
      // Only log show errors in ShowManager.
    }
    if (is_object($seasons)) {
      foreach ($seasons as $season) {
        if ($season_item = $client->getSeason($season->id)) {
          $this->seasonManager->addOrUpdateSeason($season_item, $options['force']);
          if ($episodes = $client->getEpisodes($season->id)) {
            foreach ($episodes as $episode) {
              if ($episode_item = $client->getEpisode($episode->id)) {
                $this->episodeManager->addOrUpdateEpisode($episode_item, $options['force']);
              }
            }
          }
        }
      }
    }
    if ($specials = $client->getSpecials($show_id)) {
      foreach ($specials as $special) {
        if ($special_item = $client->getSpecial($special->id)) {
          $this->specialManager->addOrUpdateSpecial($special_item, $options['force']);
        }
      }
    }
    $this->output()->writeln(dt("All content associated with this show has been imported."));
  }

  /**
   * Command to queue all Media Manager assets.
   *
   * @command pbs_media_manager:getAllAssets
   * @aliases pbs-gaa
   */
  public function getAllAssets() {
    $this->output()->writeln(dt("This command has not been implemented."));
    // TODO: Strictly speaking this is not needed, becuase shows and episodes
    // get download their own assets. However, it might be beneficial to
    // create an AssetsQueueWorker class for updating all assets.
  }

  /**
   * Command to process all (subscribed) shows, seasons, and episodes.
   *
   * @command pbs_media_manager:processAll
   * @aliases pbs-pa
   */
  public function processAll() {
    $this->showManager->updateQueue();
    $this->runQueue('pbs_media_manager.queue.shows');
    $this->seasonManager->updateQueue();
    $this->runQueue('pbs_media_manager.queue.seasons');
    $this->episodeManager->updateQueue();
    $this->runQueue('pbs_media_manager.queue.episodes');
    $this->specialManager->updateQueue();
    $this->runQueue('pbs_media_manager.queue.episodes');
    $this->output()->writeln(dt("All shows, seasons, episodes, and specials have been processed."));
  }

  /**
   * Run a specific queue by name.
   *
   * @param string $name
   *   The name of the queue to run.
   *
   * @command pbs:rq
   * @aliases pbs-rq
   */
  public function runQueue($name) {
    $worker = $this->workerManager->createInstance($name);
    $queue = $this->queueService->get($name);
    $start = microtime(TRUE);
    $count = 0;

    while ($item = $queue->claimItem()) {
      try {
        $this->logger()->info(dt('Processing item @id from @name queue.', [
          '@name' => $name,
          '@id' => $item->item_id,
        ]));
        $worker->processItem($item->data);
        $queue->deleteItem($item);
        $count++;
      }
      catch (RequeueException $e) {
        // The worker requested the task to be immediately requeued.
        $queue->releaseItem($item);
      }
      catch (SuspendQueueException $e) {
        // If the worker indicates there is a problem with the whole queue,
        // release the item.
        $queue->releaseItem($item);
        throw new \Exception($e->getMessage());
      }
    }
    $elapsed = microtime(TRUE) - $start;
    $this->logger()->success(dt('Processed @count items from the @name queue in @elapsed sec.', [
      '@count' => $count,
      '@name' => $name,
      '@elapsed' => round($elapsed, 2),
    ]));
  }

}
