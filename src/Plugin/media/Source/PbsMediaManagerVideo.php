<?php

namespace Drupal\pbs_media_manager\Plugin\media\Source;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Field\FieldTypePluginManagerInterface;
use Drupal\media\MediaSourceBase;

/**
 * Provides a media source plugin to use a Media Manager asset ID as the source.
 *
 * @MediaSource(
 *   id = "pbs_id",
 *   label = @Translation("PBS Media Manager ID"),
 *   description = @Translation("Video ID."),
 *   allowed_field_types = {"string"},
 *   default_thumbnail_filename = "video.png",
 *   deriver = "",
 *   providers = {},
 * )
 */
class PbsMediaManagerVideo extends MediaSourceBase {

  /**
   * Constructs a new class instance.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   Entity type manager service.
   * @param \Drupal\Core\Entity\EntityFieldManagerInterface $entity_field_manager
   *   Entity field manager service.
   * @param \Drupal\Core\Field\FieldTypePluginManagerInterface $field_type_manager
   *   The field type plugin manager service.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory service.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityTypeManagerInterface $entity_type_manager, EntityFieldManagerInterface $entity_field_manager, FieldTypePluginManagerInterface $field_type_manager, ConfigFactoryInterface $config_factory) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $entity_type_manager, $entity_field_manager, $field_type_manager, $config_factory);
    $this->pluginDefinition['allowed_field_types'] = ['string'];
  }

  /**
   * {@inheritdoc}
   */
  public function getMetadataAttributes() {
    return [
      'video_title' => $this->t('Title'),
      'video_duration' => $this->t('Duration'),
      'video_updated' => $this->t('Updated'),
      'public_avail_start' => $this->t('Public Availability Start'),
      'public_avail_end' => $this->t('Public Availability End'),
      'public_updated_at' => $this->t('Public Availability Updated'),
      'all_members_avail_start' => $this->t('All Members Start'),
      'all_members_avail_end' => $this->t('All Members End'),
      'all_members_updated_at' => $this->t('All Members Updated'),
      'station_members_avail_start' => $this->t('Station Members Start'),
      'station_members_avail_end' => $this->t('Station Members End'),
      'station_members_updated_at' => $this->t('Station Members Updated'),
    ];
  }

}
