<?php

namespace Drupal\pbs_media_manager\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\Plugin\Field\FieldWidget\StringTextfieldWidget;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class PbsMediaManagerShowGuidFieldWidget.
 *
 * @FieldWidget(
 *   id = "pbs_media_manager_show_guid",
 *   label = @Translation("PBS Media Manager show guid convert to show name"),
 *   field_types = {
 *     "string"
 *   },
 * )
 */
class PbsMediaManagerShowGuidFieldWidget extends StringTextfieldWidget {

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $main_widget = parent::formElement($items, $delta, $element, $form, $form_state);
    $element = $main_widget['value'];

    $wrapper_id = 'form-show-guid';
    $form['#prefix'] = '<div id="form-show-guid" class="form-show-guid-wrapper">';
    $form['#suffix'] = '</div>';
    $element['#ajax'] = [
      'event'  => 'change',
      'callback' => [$this, 'showGuidChanged'],
      'wrapper' => $wrapper_id,
      'method' => 'replace',
    ];

    return ['value' => $element];
  }

  /**
   * Callback to add default show term name from show guid value.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @return mixed
   *   Returns form element.
   */
  public function showGuidChanged(array &$form, FormStateInterface $form_state) {
    // Get the chosen playlist value.
    $triggering_element = $form_state->getTriggeringElement();
    $show_guid = $triggering_element['#value'];
    /** @var \Drupal\pbs_media_manager\ApiClient $client  */
    $client = \Drupal::service('pbs_media_manager.api_client');
    $show_title = $client->getShowTitleFromGuid($show_guid);
    if ($show_title) {
      $form['name']['widget'][0]['value']['#value'] = $show_title;
    }
    return $form;
  }

}
