<?php

namespace Drupal\pbs_media_manager\Plugin\QueueWorker;

use Drupal\Core\Logger\LoggerChannelInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Queue\QueueWorkerBase;
use Drupal\pbs_media_manager\SeasonManager;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Processes Season data from the Media Manager API to Season nodes.
 *
 * Queue items are added by cron processing.
 *
 * @QueueWorker(
 *   id = "pbs_media_manager.queue.seasons",
 *   title = @Translation("Media Manager Seasons processor"),
 *   cron = {"time" = 60},
 *   weight = 0
 * )
 *
 * @see pbs_media_manager_cron()
 * @see \Drupal\Core\Annotation\QueueWorker
 * @see \Drupal\Core\Annotation\Translation
 */
class SeasonsQueueWorker extends QueueWorkerBase implements ContainerFactoryPluginInterface {

  /**
   * PBS Media Manager logger.
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  private $logger;

  /**
   * Season manager.
   *
   * @var \Drupal\pbs_media_manager\SeasonManager
   */
  private $seasonManager;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration,
    $plugin_id,
    $plugin_definition,
    LoggerChannelInterface $logger,
    SeasonManager $season_manager
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->logger = $logger;
    $this->seasonManager = $season_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(
    ContainerInterface $container,
    array $configuration,
    $plugin_id,
    $plugin_definition
  ) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('logger.channel.pbs_media_manager'),
      $container->get('pbs_media_manager.season_manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function processItem($item): void {
    $this->seasonManager->addOrUpdateSeason($item);
  }

}
