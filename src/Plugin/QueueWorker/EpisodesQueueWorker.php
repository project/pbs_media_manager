<?php

namespace Drupal\pbs_media_manager\Plugin\QueueWorker;

use Drupal\Core\Logger\LoggerChannelInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Queue\QueueWorkerBase;
use Drupal\pbs_media_manager\EpisodeManager;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Processes Episode data from the Media Manager API to Episode nodes.
 *
 * Queue items are added by cron processing.
 *
 * @QueueWorker(
 *   id = "pbs_media_manager.queue.episodes",
 *   title = @Translation("Media Manager Episodes processor"),
 *   cron = {"time" = 60},
 *   weight = 10
 * )
 *
 * @see pbs_media_manager_cron()
 * @see \Drupal\Core\Annotation\QueueWorker
 * @see \Drupal\Core\Annotation\Translation
 */
class EpisodesQueueWorker extends QueueWorkerBase implements ContainerFactoryPluginInterface {

  /**
   * PBS Media Manager logger.
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  private $logger;

  /**
   * Episode manager.
   *
   * @var \Drupal\pbs_media_manager\EpisodeManager
   */
  private $episodeManager;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration,
    $plugin_id,
    $plugin_definition,
    LoggerChannelInterface $logger,
    EpisodeManager $episode_manager
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->logger = $logger;
    $this->episodeManager = $episode_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(
    ContainerInterface $container,
    array $configuration,
    $plugin_id,
    $plugin_definition
  ) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('logger.channel.pbs_media_manager'),
      $container->get('pbs_media_manager.episode_manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function processItem($item): void {
    $this->episodeManager->addOrUpdateEpisode($item);
  }

}
