<?php

use Drupal\Core\Entity\EntityInterface;
/**
 * @file
 * Describes methods to change Show, Season, and Episode nodes before saving.
 */

/**
 * Perform alterations to the operation performed on entities.
 *
 * To indicate that a show, season, episode, or asset was ignored due to custom
 * logic set $operation to 'ignored'.
 *
 * @param string $operation
 *   The operation to be performed: created, updated, skipped, or configured.
 * @param array $context
 *   An associative array with the following elements:
 *   - item: The PBS Media Manager API item (shows, season, episode, or asset).
 *   - entity: The Drupal entity (shows, season, episode, or video media).
 */
function hook_pbs_media_manager_operation_alter(&$operation, array $context) {

  $ignored_seasons = [
    '4feda6a9-3331-42cd-b09c-040c8dcfac65',
    'baf32257-88da-4462-a68b-35ba2ae28e94',
    '7a11611c-47a3-42db-8d87-ec156671dbbf',
  ];
  $item = $context['item'];

  // Ignore certain seasons based on guid.
  if ($item->type == 'season' && in_array($item->id, $ignored_seasons)) {
    $operation = 'ignored';
  }
  // Also ignore the episodes in the ignored seasons.
  elseif ($item->type == 'episode' && !empty($item->attributes->season->id)) {
    if (in_array($item->attributes->season->id, $ignored_seasons)) {
      $operation = 'ignored';
    }
  }
  // And ignore the assets in the episodes in the ignored seasons.
  elseif ($item->type == 'asset' && !empty($item->attributes->parent_tree->attributes->season->id)) {
    if (in_array($item->attributes->parent_tree->attributes->season->id, $ignored_seasons)) {
      $operation = 'ignored';
    }
  }
}

/**
 * Example of how to alter a show node before it is created/updated.
 *
 * One way is to implement hook_entity_presave().
 */
function my_module_entity_presave(EntityInterface $entity) {

  /** @var \Drupal\Core\Config\ConfigFactoryInterface $config_factory */
  $config_factory = \Drupal::configFactory();
  $config = $config_factory->get('pbs_media_manager.settings');
  $show_type = $config->get('shows.drupal_show_content');

  // In this example, the "Local Content" boolean field is set to TRUE if the
  // the value of attributes->audience->attributes->call_sign in Media Manager
  // is "WGTV".
  if ($entity->bundle() == $show_type &&
    !empty($entity->get('field_call_sign')->getValue())) {
    $call_sign = $entity->get('field_call_sign')->getValue();
    if ($call_sign[0]['value'] == "WGTV") {
      $entity->set('field_local_content', TRUE);
    }
  }

}
